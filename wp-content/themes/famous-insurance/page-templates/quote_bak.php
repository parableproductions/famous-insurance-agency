<script>
    jQuery(document).ready(function($) {
        $('.nav a').click(function(e) {
            e.preventDefault()
            $(this).tab('show')
        })
        $('a.scroll').on('click', function(e) {
            var href = $(this).attr('href');
            $('html, body').animate({
                scrollTop: $(href).offset().top
            }, 'slow');
            e.preventDefault();
        });
    });

</script>


<script language='JavaScript1.1' async src='//pixel.mathtag.com/event/js?mt_id=1380928&mt_adid=218974&mt_exem=&<!='></script>

<?php $top_section_bg = get_sub_field( 'top_section_bg' ); ?>

<?php 
    $image = get_sub_field('top_section_bg' );
    if( !empty($image) ): 
    // vars
    $title = $image['title'];
    $alt = $image['alt'];

	// image
    $size = 'full';
	$thumb = $image['sizes'][ $size ];
	$width = $image['sizes'][ $size . '-width' ];
	$height = $image['sizes'][ $size . '-height' ];
    ?>

<section class="no-padding-top">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <img src="<?php echo $top_section_bg['url']; ?>">
            </div>
        </div>
    </div>
</section>
<?php endif; ?>

<section>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2 class="text-uppercase text-center font-xbold">
                    <?php the_sub_field( 'page_title' ); ?>
                </h2>
                <h3 class="italic text-center">
                    <?php the_sub_field( 'page_slogan' ); ?>
                </h3>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="font-light">
                    <?php the_sub_field( 'page_content' ); ?>
                </div>
            </div>
        </div>
    </div>
</section>

<section>
    <div class="container">
        <div class="row">
            <ul class="nav nav-pills mb-3 row justify-content-md-center" id="pills-tab" role="tablist">
                <?php
$args = array(
    'orderby' => 'name',
    'post_type'=>'insurance',
    'exclude'    => '9'
);
$btncount = 0;

$range = new WP_Query( $args );
if( $range->have_posts() ) :
?>

                <?php
      while( $range->have_posts() ) :
        $range->the_post();
        ?>
                <li class="nav-item col-md-2 col-6 text-center">
                    <a class="nav-link font-light scroll" id="<?php echo $btncount ?>-tab" data-toggle="tab" href="#tab<?php echo $btncount ?>" role="tab" aria-controls="<?php echo $btncount ?>" aria-selected="false">
                        <?php $insurance_icon = get_field( 'insurance_icon' ); ?>
                        <?php if ( $insurance_icon ) { ?>
                        <img src="<?php echo $insurance_icon['url']; ?>" alt="<?php echo $insurance_icon['alt']; ?>" />
                        <?php } ?>
                        <?php the_title(); ?>
                    </a>
                </li>
                <?php $btncount++; ?>
                <?php endwhile; ?>
                <?php endif; ?>
                <?php wp_reset_postdata(); ?>
            </ul>
        </div>
    </div>
</section>

<div class="tab-content" id="pills-tabContent">
    <?php
$args = array(
    'orderby' => 'name',
    'post_type'=>'insurance'
);
$tabcount = 0;

    $range = new WP_Query( $args );
if( $range->have_posts() ) :
?>

    <?php
      while( $range->have_posts() ) :
        $range->the_post();
        ?>

    <div class="tab-pane fade" id="tab<?php echo $tabcount ?>" role="tabpanel" aria-labelledby="<?php echo $tabcount ?>-tab">
        <!-- Tab -->
        <section class="bg-grey pt-4">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h3 class="text-uppercase text-center font-xbold">
                            <?php the_title(); ?>
                        </h3>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 text-center">
                        <p class="font-light">
                            <?php the_field('quote_form_title'); ?>
                        </p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 quote-form">
                        <?php $quoteforms = get_field( 'quote_form' ); ?>
                        <?php if ( $quoteforms ): ?>
                        <?php echo $quoteforms; ?>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </section>
        <!-- /Tab -->
    </div>
    <?php $tabcount++; ?>
    <?php endwhile; ?>
    <?php endif; ?>
    <?php wp_reset_postdata(); ?>
</div>
</div>

<!-- Red Section -->
<section class="section-red">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2 class="text-uppercase text-center font-xbold">
                    <?php the_sub_field( 'featured_section_title' ); ?>
                </h2>
                <h3 class="italic text-center">
                    <?php the_sub_field( 'featured_section_slogan' ); ?>
                </h3>
                <hr/ class="hr-white">
            </div>
        </div>

        <?php if( have_rows('featured_section_repeater') ): ?>
        <div class="row">
            <?php while( have_rows('featured_section_repeater') ): the_row(); 
                // vars
                $title = get_sub_field('featured_section_single_title');
                $content = get_sub_field('featured_section_single_content');
                ?>
            <div class="col-md-4 text-center">
                <p class="text-uppercase font-bold title">
                    <?php echo $title; ?>
                </p>
                <p class="font-light">
                    <?php echo $content; ?>
                </p>
            </div>
            <?php endwhile; ?>
        </div>
        <?php endif; ?>
    </div>
</section>
<!-- /Red Section -->
