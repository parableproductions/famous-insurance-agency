<?php
/*
Template Name: Kiipix Page
*/
?>

<?php get_header(); ?>

<section>
    <div style="background-image:url('http://dev.famousinsurance.com.au/wp-content/themes/famous-insurance/assets/img/kiipix/KiiPix_Background.jpg'); background-repeat: repeat; background-size: auto;">
        
        <div class="container">
            <div class="row">
                <div class="col-lg-3">
                    <img src="http://dev.famousinsurance.com.au/wp-content/themes/famous-insurance/assets/img/kiipix/Logo_KiiPix_425x250.png" />
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <img src="http://dev.famousinsurance.com.au/wp-content/themes/famous-insurance/assets/img/kiipix/KiiPix_top_1170x350.jpg" />
                </div>
            </div>
            <div class="row">
                <div class="col-12 pt-4">
                    <img src="http://dev.famousinsurance.com.au/wp-content/themes/famous-insurance/assets/img/kiipix/KiiPix_photostrip_1170x200.jpg" />
                </div>
            </div>
            <div class="row pb-4">
                <div class="col-lg-6 col-12 pt-4">
                    <img src="http://dev.famousinsurance.com.au/wp-content/themes/famous-insurance/assets/img/kiipix/Video1_750x400.jpg" />
                </div>
                <div class="col-lg-6 col-12 pt-4">
                    <img src="http://dev.famousinsurance.com.au/wp-content/themes/famous-insurance/assets/img/kiipix/Video2_750x400.jpg" />
                </div>
            </div>
            <div class="row" style="background-color: white">
	            <div class="col-xs-12 col-sm-4 justify-content-center align-self-center mx-auto">
		            <img src="http://dev.famousinsurance.com.au/wp-content/themes/famous-insurance/assets/img/kiipix/Image_TakeAnywhere_750x530.jpg" />
		        </div>
	            <div class="col-xs-12 col-sm-8 justify-content-center align-self-center mx-auto">
		            <img src="https://c2.tomy.com/sites/all/themes/kiipix/images/Headline_TakeIt_425x50.png" style="width: 350px;" />
		            No batteries to replace and no cords to plug in. Add in the compact fold, and KiiPix is an easy travel companion that lets you create in-the-moment souvenirs on all your adventures.
		            <img src="https://c2.tomy.com/sites/all/themes/kiipix/images/3Circles_Anywhere_425x120.png" style="width: 250px" />
		        </div>
            </div>
            <div class="pt-4 pb-4">
                <div class="row" style="background-color: white">
                    <div class="col-xs-12 col-sm-8 justify-content-center align-self-center mx-auto">
    		            <img src="https://c2.tomy.com/sites/all/themes/kiipix/images/Headline_Filter_425x50.png" style="width: 350px;" />
    		            Whether it’s your favorite face swap pic or that selfie you took with the bunny ear filter, any picture on your phone is now printable with KiiPix.
    		        </div>
    		        <div class="col-xs-12 col-sm-4 justify-content-center align-self-center mx-auto">
    		            <img src="http://dev.famousinsurance.com.au/wp-content/themes/famous-insurance/assets/img/kiipix/Image_Filter_750x530.jpg" />
    		        </div>
                </div>
            </div>
            <div class="row" style="background-color: white">
	            <div class="col-xs-12 col-sm-4 justify-content-center align-self-center mx-auto">
		            <img src="http://dev.famousinsurance.com.au/wp-content/themes/famous-insurance/assets/img/kiipix/Image_ProTips_750x530.jpg" />
		        </div>
	            <div class="col-xs-12 col-sm-8 justify-content-center align-self-center mx-auto">
		            <img src="https://c2.tomy.com/sites/all/themes/kiipix/images/Headline_ProTips_425x50.png" style="width: 350px;" />
		            For best results, set your phone screen brightness to maximum and turn off auto-lock. Use screen lock to keep pictures from rotating. Align the KiiPix frame to your phone and place it on top of KiiPix. Then use the mirror to make sure the photo is lined up the way you want it.
		            <img src="https://c2.tomy.com/sites/all/themes/kiipix/images/3Circles_Tips_425x120.png" style="width: 250px" />
		            From your smartphone to a photo in minutes! Download the 
		            <a href="https://tomy.com/sites/default/files/content/en_US/tomy/kiipix/KiiPix_Quick_Start_Guide_EN.pdf">Quick Start Guide
		            </a>
		        </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <h3 class="text-center pt-4 pb-4" style="color: white">Kiipix - Found At The Following Retailers</h3>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-2 col-6">
                    <a href="https://www.amazon.com.au/s/ref=nb_sb_noss_2?url=search-alias%3Daps&field-keywords=kiipix">
                    <img src="http://dev.famousinsurance.com.au/wp-content/themes/famous-insurance/assets/img/kiipix/1.png" />
                    </a>
                </div>
                <div class="col-lg-2 col-6">
                    <img src="http://dev.famousinsurance.com.au/wp-content/themes/famous-insurance/assets/img/kiipix/2.png" />
                </div>
                <div class="col-lg-2 col-6">
                    <img src="http://dev.famousinsurance.com.au/wp-content/themes/famous-insurance/assets/img/kiipix/3.png" />
                </div>
                <div class="col-lg-2 col-6">
                    <img src="http://dev.famousinsurance.com.au/wp-content/themes/famous-insurance/assets/img/kiipix/4.png" />
                </div>
                <div class="col-lg-2 col-6">
                    <a href="https://www.catch.com.au/search?query=kiipix&search_src=topbar">
                    <img src="http://dev.famousinsurance.com.au/wp-content/themes/famous-insurance/assets/img/kiipix/5.png" />
                    </a>
                </div>
                <div class="col-lg-2 col-6">
                    <a href="https://www.kgelectronic.com.au/?kw=kiipix">
                    <img src="http://dev.famousinsurance.com.au/wp-content/themes/famous-insurance/assets/img/kiipix/6.png" />
                    </a>
                </div>
            </div>
            <div class="row pt-5">
                <div class="col-lg-2 col-6">
                    <a href="https://www.kmart.com.au/product/kiipix-photo-printer/2200301">
                    <img src="http://dev.famousinsurance.com.au/wp-content/themes/famous-insurance/assets/img/kiipix/7.png" />
                    </a>
                </div>
                <div class="col-lg-2 col-6">
                    <img src="http://dev.famousinsurance.com.au/wp-content/themes/famous-insurance/assets/img/kiipix/8.png" />
                </div>
                <div class="col-lg-2 col-6">
                    <img src="http://dev.famousinsurance.com.au/wp-content/themes/famous-insurance/assets/img/kiipix/9.png" />
                </div>
                <div class="col-lg-2 col-6">
                    <img src="http://dev.famousinsurance.com.au/wp-content/themes/famous-insurance/assets/img/kiipix/10.png" />
                </div>
                <div class="col-lg-2 col-6">
                    <img src="http://dev.famousinsurance.com.au/wp-content/themes/famous-insurance/assets/img/kiipix/11.png" />
                </div>
            </div>
        </div>
    </div>
</section>


<?php get_footer(); ?>
