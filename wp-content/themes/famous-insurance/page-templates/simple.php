<?php $top_section_bg = get_sub_field( 'top_section_bg' ); ?>

<?php 
    $image = get_sub_field('top_section_bg' );
    if( !empty($image) ): 
    // vars
    $title = $image['title'];
    $alt = $image['alt'];

	// image
	$thumb = $image['sizes'][ $size ];
	$width = $image['sizes'][ $size . '-width' ];
	$height = $image['sizes'][ $size . '-height' ];
    ?>

<section class="no-padding-top">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <img src="<?php echo $top_section_bg['url']; ?>">
            </div>
        </div>
    </div>
</section>
<?php endif; ?>

<section>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2 class="text-uppercase text-center font-xbold">
                    <?php the_sub_field( 'title' ); ?>
                </h2>
            </div>
        </div>
    </div>
    </scetion>

    <section id="content">
        <div class="container">
            <div class="row">
                <div class="font-light">
                    <?php the_sub_field( 'content' ); ?>
                </div>
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container -->
    </section>
