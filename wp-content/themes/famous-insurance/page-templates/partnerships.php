<?php $top_section_bg = get_sub_field( 'top_section_bg' ); ?>

<?php 
    $image = get_sub_field('top_section_bg' );
    if( !empty($image) ): 
    // vars
    $title = $image['title'];
    $alt = $image['alt'];

	// image
    $size = 'full';
	$thumb = $image['sizes'][ $size ];
	$width = $image['sizes'][ $size . '-width' ];
	$height = $image['sizes'][ $size . '-height' ];
    ?>

<section class="no-padding-top">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <img src="<?php echo $top_section_bg['url']; ?>">
            </div>
        </div>
    </div>
</section>
<?php endif; ?>

<section>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2 class="text-uppercase text-center font-xbold">
                    <?php the_sub_field( 'page_title' ); ?>
                </h2>
            </div>
        </div>
    </div>
</section>

<section id="content">
    <div class="container">
        <div class="row row-eq-height">

            <?php $loop = new WP_Query( array( 'post_type' => 'partnerships', 'orderby' => 'date', 'order' => 'DES', 'posts_per_page' => '99' ) ); ?>
            <?php while( $loop->have_posts() ) : $loop->the_post(); ?>
            <div class="col-md-4">
                <div class="card">

                    <?php
                if ( has_post_thumbnail() ) {
                    the_post_thumbnail('large', array( 'class' => 'card-img-top img-fluid' ));
                } ?>
                        <div class="card-body">
                            <h5 class="card-title">
                                <?php the_title ();?>
                            </h5>
                            <a href="<?php the_field('partnership_url'); ?>" target="_blank" class="btn btn-quote btn-block text-uppercase">Find Out More</a>
                        </div>
                </div>
            </div>

            <?php $counter++;
                  if ($counter % 3 == 0) {
                  echo '</div><div class="row row-eq-height pt-4">';
                } ?>
            <?php endwhile; ?>


        </div>

    </div>
    </div>
</section>
