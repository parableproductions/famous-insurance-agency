<?php $top_section_bg = get_sub_field( 'top_section_bg' ); ?>

<?php 
    $image = get_sub_field('top_section_bg' );
    if( !empty($image) ): 
    // vars
    $title = $image['title'];
    $alt = $image['alt'];

	// image
    $size = 'full';
	$thumb = $image['sizes'][ $size ];
	$width = $image['sizes'][ $size . '-width' ];
	$height = $image['sizes'][ $size . '-height' ];
    ?>

<section class="no-padding-top">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <img src="<?php echo $top_section_bg['url']; ?>">
            </div>
        </div>
    </div>
</section>
<?php endif; ?>

<section>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2 class="text-uppercase text-center font-xbold">
                    <?php the_sub_field( 'page_title' ); ?>
                </h2>
                <h3 class="italic text-center">
                    <?php the_sub_field( 'page_slogan' ); ?>
                </h3>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <h4 class="text-uppercase font-bold">Contact Us Directly</h4>
                <p><strong>Phone</strong> 1300 32 66 87<br />
                <strong>Fax</strong> 1300 30 32 06<br />
                <strong>Address</strong> P.O. Box 6244, Baulkham Hills, NSW 2153<br />
                <strong>Email</strong> &#105;&#110;&#102;&#111;&#064;&#102;&#097;&#109;&#111;&#117;&#115;&#105;&#110;&#115;&#117;&#114;&#097;&#110;&#099;&#101;&#046;&#099;&#111;&#109;&#046;&#097;&#117;</p>
            </div>
        </div>
    </div>
</section>


<section>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2 class="text-uppercase text-center font-xbold"><span class="text-red">Contact</span> Form</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <?php echo do_shortcode('[contact-form-7 id="207" title="Contact Famous Insurance"]'); ?>
            </div>
        </div>
    </div>
</section>
