<?php require_once 'nevdis_api.php'; ?>

<section class="no-padding-top">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <img src="https://famousinsurance.com.au/wp-content/uploads/2018/09/RequestAQuoteHero.jpg">
            </div>
        </div>
    </div>
</section>

<section>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2 class="text-uppercase text-center font-xbold">
                    Autofill By <span class="text-red">Registration</span></h2>
                <h3 class="italic text-center">
                    You can get your vehicle covered quickly and easily</h3>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="font-light">
                    <p style="text-align: center;">Filter by vehicle make, model and year then select your vehicle to autofill your insurance quote.</p>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">

                <form action="" method="post">
                    <div class="form-group">
                        <select class="form-control" name="statelookup" id="statelookup">
                            <option value="VIC">Victoria</option>
                            <option value="NSW">New South Wales</option>
                            <option value="QLD">Queensland</option>
                            <option value="SA">South Australia</option>
                            <option value="WA">Western Australia</option>
                            <option value="TAS">Tasmania</option>
                            <option value="ACT">Australian Capital Territory</option>
                            <option value="NT">Northern Territory</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" name="platelookup" id="platelookup" placeholder="Enter License Plate No.">
                    </div>
                    <button type="submit" name="submit" value="submit" class="btn btn-quote btn-block text-uppercase">Next Step/Add Details</button>
                </form>

            </div>
        </div>
    </div>
</section>

<?php
 if (isset($_POST['submit'])) { ?>

<?php
     
if( !empty( $_GET['type'] ) ) {
$_drivetype = $_GET['type'];
}                             
                               
$_SESSION['statelookup'] = $_POST['statelookup'];
$_state = $_SESSION['statelookup'];

$_SESSION['platelookup'] = $_POST['platelookup'];
$_plate = $_SESSION['platelookup'];
     
$_SESSION['vinlookup'] = $_POST['vinlookup'];
$_vin = $_SESSION['vinlookup']; 

$query = new nevdis_api;
$search = array(
	'state'	=> $_state,
	'plate'	=> $_plate,
);
$result = $query->request_data($search);
     
$_colour = ($result["colour"]);
     
$_plate = array_column($result, 'number'); 
$_plate = implode(",", $_plate);
     
$_make = array_column($result, 'make'); 
$_make = implode(",", $_make);
     
$_model = array_column($result, 'model'); 
$_model = implode(",", $_model);
     
$_year = array_column($result, 'buildYear'); 
$_year = implode(",", $_year); 

$_cylinders = array_column($result, 'cylinders'); 
$_cylinders = implode(",", $_cylinders);         

header('Location: https://famousinsurance.com.au/get-a-quote/' . $_drivetype .'/?make='. $_make .'&model='. $_model .'&make-year='. $_year .'&description='. $_colour .'&registration-number='. $_plate .'&engine='. $_cylinders .'%20cylinders' );   
     
?>

<?php } ?>
