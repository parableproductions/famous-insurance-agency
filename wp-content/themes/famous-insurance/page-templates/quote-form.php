<?php $top_section_bg = get_sub_field( 'top_section_bg' ); ?>

<?php
    $image = get_sub_field('top_section_bg' );
    if( !empty($image) ):
    // vars
    $title = $image['title'];
    $alt = $image['alt'];

	// image
	$thumb = $image['sizes'][ $size ];
	$width = $image['sizes'][ $size . '-width' ];
	$height = $image['sizes'][ $size . '-height' ];
    ?>

<section class="no-padding-top">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <img src="<?php echo $top_section_bg['url']; ?>">
            </div>
        </div>
    </div>
</section>
<?php endif; ?>

<section>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2 class="text-uppercase text-center font-xbold">
                    Get A <span class="text-red">Quote</span></h2>
                <h3 class="italic text-center">
                    You can get your vehicle covered quickly and easily
                </h3>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="font-light">
                    Famous Insurance provide insurance solutions for classic and collectable cars, high performance, exotic, sports and vintage vehicles, as well as motorbikes, scooters and dirt bikes. Select a vehicle below and fill in the quote form to get an insurance quote on your vehicle.
                </div>
            </div>
        </div>
    </div>
</section>

<section class="bg-grey pt-4">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h3 class="text-uppercase text-center font-xbold">
                    <?php the_sub_field( 'title' ); ?>
                </h3>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 text-center">
                <p class="font-light">
                    Fill in the form below to apply for an insurance quote
                </p>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 quote-form">
                <?php the_sub_field( 'content' ); ?>
            </div>
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container -->
</section>


<!--<script>   
    function getinformation(){
        var fn = document.getElementById("contact-firstname").value;
        var ln = document.getElementById("contact-lastname").value;
        document.write("Hello, " + fn + - + ln);
        //document.write(" <script src=\"\/\//pixel.mathtag.com/event/js?mt_id=1380929&mt_adid=218974&mt_exem=&mt_excl=&v1="+fn+"&v2="+ln+"\"><\/script>");
    }
</script>   -->

<script>
var placeSearch, autocomplete;

var componentForm = {
  postal_code: 'short_name'
};

function initAutocomplete() {
  // Create the autocomplete object, restricting the search predictions to
  // geographical location types.
  autocomplete = new google.maps.places.Autocomplete(
      document.getElementById('parking-address'), {types: ['geocode'], componentRestrictions: {country: 'au'}});

  // Avoid paying for data that you don't need by restricting the set of
  // place fields that are returned to just the address components.
  autocomplete.setFields(['address_component']);

  // When the user selects an address from the drop-down, populate the
  // address fields in the form.
  autocomplete.addListener('place_changed', fillInAddress);
}

function fillInAddress() {
  // Get the place details from the autocomplete object.
  var place = autocomplete.getPlace();

  for (var component in componentForm) {
    document.getElementById(component).value = '';
    document.getElementById(component).disabled = false;
  }

  // Get each component of the address from the place details,
  // and then fill-in the corresponding field on the form.
  for (var i = 0; i < place.address_components.length; i++) {
    var addressType = place.address_components[i].types[0];
    if (componentForm[addressType]) {
      var val = place.address_components[i][componentForm[addressType]];
      document.getElementById(addressType).value = val;
    }
  }
}

// Bias the autocomplete object to the user's geographical location,
// as supplied by the browser's 'navigator.geolocation' object.
function geolocate() {
  if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(function(position) {
      var geolocation = {
        lat: position.coords.latitude,
        lng: position.coords.longitude
      };
      var circle = new google.maps.Circle(
          {center: geolocation, radius: position.coords.accuracy});
      autocomplete.setBounds(circle.getBounds());
    });
  }
}

/*const wrapper = document.querySelector('.quote-form'),
    form = wrapper.querySelectorAll('.wpcf7'),
    submitInput = form[0].guerySelector('input[type="submit"]');

 function getDataFrom(){
     //e.preventDefault();
     var formData = new FormData(form[0]);
     alert(formData.get('contact-firstname') + ' - ' + formData.get('contact-lastname') );

 }

document.addEventListener('DOMContentLoaded', function(){
    submitInput.addEventListener('click', getDataFrom, false);
}, false);*/


/*function getinformation(){
    var fn = document.getElementById("contact-firstname").value;
    //document.write("Hello, " + fn);
    document.write("<script src=\"\/\//pixel.mathtag.com/event/js?mt_id=1380929&mt_adid=218974&mt_exem=&mt_excl=&v1="+fn+"\"><\/script>");
    
}*/


</script>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDiw0qgT7fKH1YQKTbRZ2WHKxUYJo4AGog&libraries=places&callback=initAutocomplete" async defer></script>

