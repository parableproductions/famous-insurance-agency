<?php $top_section_bg = get_sub_field( 'top_section_bg' ); ?>

<?php 
    $image = get_sub_field('top_section_bg' );
    if( !empty($image) ): 
    // vars
    $title = $image['title'];
    $alt = $image['alt'];

	// image
	$thumb = $image['sizes'][ $size ];
	$width = $image['sizes'][ $size . '-width' ];
	$height = $image['sizes'][ $size . '-height' ];
    ?>

<section class="no-padding-top">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <img src="<?php echo $top_section_bg['url']; ?>">
            </div>
        </div>
    </div>
</section>
<?php endif; ?>

<section>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2 class="text-uppercase text-center font-xbold">
                    <?php the_sub_field( 'page_title' ); ?>
                </h2>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="font-light text-center">
                    <?php the_sub_field( 'page_description' ); ?>
                </div>
            </div>
        </div>
    </div>
</section>


<!-- Policy Documents Loop -->
<section>
    <div class="container">
        <ul class="nav nav-pills mb-3 row justify-content-md-center" id="pills-tab" role="tablist">
            <?php
$args = array(
    'orderby' => 'name',
    'taxonomy'=>'insurance_type',
    'parent' => 0
);
$categories = get_categories( $args );
$btncount = 0;
foreach ( $categories as $category ) { ?>
            <li class="nav-item col-md-2 text-center">
                <a class="nav-link font-light" id="<?php echo $btncount ?>-tab" data-toggle="pill"
                    href="#<?php echo $btncount ?>" role="tab" aria-controls="<?php echo $btncount ?>"
                    aria-selected="false">
                    <?php $insurance_icon = get_field( 'insurance_icon', $category ); ?>
                    <?php if ( $insurance_icon ) { ?>
                    <img src="<?php echo $insurance_icon['url']; ?>" alt="<?php echo $insurance_icon['alt']; ?>" />
                    <?php } ?>
                    <?php echo $category->name ?>
                </a>
            </li>
            <?php $btncount++; ?>
            <?php } ?>
            <?php wp_reset_postdata(); ?>
        </ul>
    </div>
</section>

<div class="tab-content" id="pills-tabContent">
    <?php
$args = array(
    'orderby' => 'name',
    'taxonomy'=>'insurance_type',
    'parent' => 0
);
$categories = get_categories( $args );
$tabcount = 0;
foreach ( $categories as $category ) { ?>
    <div class="tab-pane fade" id="<?php echo $tabcount ?>" role="tabpanel"
        aria-labelledby="<?php echo $tabcount ?>-tab">
        <!-- Tab -->
        <section>
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h3 class="text-uppercase text-center font-xbold">
                            <?php echo $category->name ?>
                        </h3>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <?php $attached_documents = get_field( 'attached_documents', $category ); ?>
                        <?php if ( $attached_documents ): ?>
                        <ul class="list-group list-group-flush">
                            <?php foreach ( $attached_documents as $post ):  ?>
                            <?php setup_postdata ( $post ); ?>
                            <li class="list-group-item">
                                <a class="text-red" href="<?php the_field('document_upload'); ?>" target="_blank">
                                    <i class="fal fa-file-pdf"></i> <?php the_field('document_title'); ?>
                                </a>
                            </li>
                            <?php endforeach; ?>
                            <?php wp_reset_postdata(); ?>
                        </ul>

                <!-- Family and Domestic Violence Policy -->
                <div class="row mt-4">
                    <div class="col-md-12">
                    <?php echo $category->description ?>

                    </div>
                </div>
                <!-- //Family and Domestic Violence Policy -->

                        <?php endif; ?>
                    </div>
                </div>


            </div>
        </section>
        <!-- /Tab -->
    </div>
    <?php $tabcount++; ?>
    <?php } ?>
    <?php wp_reset_postdata(); ?>
</div>
<!-- /Policy Documents Loop -->
<section>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2 class="text-uppercase text-center font-xbold">
                    Your passion covered
                </h2>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <h3 class="italic text-center">
                    <p>Insurance designed with your vehicle in mind</p>
                </h3>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="font-light text-center">
                    <p>We care about what you care about. So whether you want to insure your car or your motorbike,
                        Famous Insurance has a policy that will protect it properly. If you have any questions, we’re
                        always happy to chat. Just call us on 1300 FAMOUS (1300 32 66 87).</p>
                </div>
            </div>
        </div>
    </div>
</section>


<!-- 
<section>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h4 class="text-uppercase text-center font-xbold">
                Family and Domestic Violence Policy
                </h4>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="font-light text-center">
                    <p>Famous recognises the safety of those experiencing family or domestic violence is paramount, and that it takes courage to talk openly about your situation. Famous has deployed specialised training to recognise signs of family and/or domestic violence and are committed to having a conversation with you about how we can support you during these difficult times. We’ll ensure your privacy is protected, and make certain you are supported in a sensitive and compassionate way and will arrange access to financial hardship assistance if necessary.</p>
                </div>
            </div>
        </div>
    </div>
</section> -->