<?php $top_section_bg = get_sub_field( 'top_section_bg' ); ?>

<?php 
    $image = get_sub_field('top_section_bg' );
    if( !empty($image) ): 
    // vars
    $title = $image['title'];
    $alt = $image['alt'];

	// image
    $size = 'full';
	$thumb = $image['sizes'][ $size ];
	$width = $image['sizes'][ $size . '-width' ];
	$height = $image['sizes'][ $size . '-height' ];
    ?>

<section class="no-padding-top">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <img src="<?php echo $top_section_bg['url']; ?>">
            </div>
        </div>
    </div>
</section>
<?php endif; ?>

<section>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2 class="text-uppercase text-center font-xbold">
                    <?php the_sub_field( 'about_us_title' ); ?>
                </h2>
                <h3 class="italic text-center">
                    <?php the_sub_field( 'about_us_slogan' ); ?>
                </h3>
            </div>
        </div>
    </div>
</section>

<section id="content">
    <div class="container">
        <div class="row">
            <h2 class="page-title screen-reader-text">
                <?php single_post_title(); ?>
            </h2>
            <div class="font-light">
                <?php the_sub_field( 'content' ); ?>
            </div>
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container -->
</section>

<!-- Red Section -->
<section class="section-red">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2 class="text-uppercase text-center font-xbold">
                    <?php the_sub_field( 'featured_section_title' ); ?>
                </h2>
                <h3 class="italic text-center">
                    <?php the_sub_field( 'featured_section_slogan' ); ?>
                </h3>
                <hr/ class="hr-white">
            </div>
        </div>

        <?php if( have_rows('featured_section_repeater') ): ?>
        <div class="row">
            <?php while( have_rows('featured_section_repeater') ): the_row(); 
                // vars
                $title = get_sub_field('featured_section_single_title');
                $content = get_sub_field('featured_section_single_content');
                ?>
            <div class="col-md-4 text-center">
                <p class="text-uppercase font-bold title">
                    <?php echo $title; ?>
                </p>
                <p class="font-light">
                    <?php echo $content; ?>
                </p>
            </div>
            <?php endwhile; ?>
        </div>
        <?php endif; ?>
    </div>
</section>
<!-- /Red Section -->