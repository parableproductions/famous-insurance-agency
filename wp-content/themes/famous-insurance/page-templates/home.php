<?php $top_section_bg = get_sub_field( 'top_section_bg' ); ?>

<?php 
    $image = get_sub_field('top_section_bg' );
    if( !empty($image) ): 
    // vars
    $title = $image['title'];
    $alt = $image['alt'];

	// image
	$thumb = $image['sizes'][ $size ];
	$width = $image['sizes'][ $size . '-width' ];
	$height = $image['sizes'][ $size . '-height' ];
    ?>

<section class="pt-0">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <?php masterslider(4); ?>
            </div>
        </div>
    </div>
</section>
<?php endif; ?>
<!-- /Header -->

<!-- NOTICE -->
<section>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <?php
    $notice = get_sub_field('notice');
    echo $notice;
?>
            </div>
        </div>
    </div>
</section>
<!-- /Notice -->

<!-- What We Insure -->
<section>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h3 class="text-uppercase text-center font-xbold">
                    <?php the_sub_field( 'first_content_section_title' ); ?>
                </h3>
                <p class="font-light">
                    <?php the_sub_field( 'first_content_section_content' ); ?>
                </p>
            </div>
        </div>

        <?php
$args = array(
  'post_type'   => 'insurance',
  'post_status' => 'publish',
  'posts_per_page' => -1
 );
 
$range = new WP_Query( $args );
if( $range->have_posts() ) :
?>
        <div class="customer-logos">
            <?php
      while( $range->have_posts() ) :
        $range->the_post();
        ?>
            <div class="slide">
                <?php $insurance_icon = get_field( 'insurance_icon' ); ?>
                <?php if ( $insurance_icon ) { ?>
                <a class="insurance-icon" href="<?php the_permalink(); ?>">
                    <img class="img-fluid pb-2" src="<?php echo $insurance_icon['url']; ?>"
                        alt="<?php echo $insurance_icon['alt']; ?>" />
                    <?php } ?>
                    <p class="font-light text-center">
                        <?php the_title(); ?>
                    </p>
                </a>
            </div>
            <?php endwhile; ?>
        </div>
        <?php endif; ?>

    </div>
</section>
<!-- /What We Insure -->

<!-- Red Section -->
<section class="section-red">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2 class="text-uppercase text-center font-xbold">
                    <?php the_sub_field( 'featured_section_title' ); ?>
                </h2>
                <h3 class="italic text-center">
                    <?php the_sub_field( 'featured_section_slogan' ); ?>
                </h3>
                <hr / class="hr-white">
            </div>
        </div>

        <?php if( have_rows('featured_section_repeater_one') ): ?>
        <div class="row">
            <?php while( have_rows('featured_section_repeater_one') ): the_row(); 
                // vars
                $title = get_sub_field('featured_section_single_title');
                $content = get_sub_field('featured_section_single_content');
                ?>
            <div class="col-md-4 text-center">
                <h4 class="text-uppercase font-bold">
                    <?php echo $title; ?>
                </h4>
                <p class="font-light">
                    <?php echo $content; ?>
                </p>
            </div>
            <?php endwhile; ?>
        </div>
        <?php endif; ?>
    </div>
</section>
<!-- /Red Section -->

<section>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <h2 class="text-uppercase text-center font-xbold">
                    <?php the_sub_field( 'why_choose_us_title' ); ?>
                </h2>
                <h3 class="italic text-center">
                    <?php the_sub_field( 'why_choose_us_slogan' ); ?>
                </h3>
                <hr / class="hr-white">
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <?php 
                    $image = get_sub_field('why_choose_us_image');
                    $width = get_sub_field('why_choose_us_image_width');
                    if( !empty($image) ): ?>
                <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>"
                    width="<?php echo $width ?>%" />
                <?php endif; ?>
            </div>
            <div class="col-md-6 justify-content-center align-self-center">

                <?php if( have_rows('why_choose_us_repeater') ): ?>
                <div class="row">
                    <?php while( have_rows('why_choose_us_repeater') ): the_row(); 
                // vars
                $title = get_sub_field('why_choose_us_title');
                $content = get_sub_field('why_choose_us_content');
                ?>
                    <div class="col-md-6 text-center">
                        <h4 class="text-uppercase text-center font-bold">
                            <?php echo $title; ?>
                        </h4>
                        <p class="font-light">
                            <?php echo $content; ?>
                        </p>
                    </div>
                    <?php endwhile; ?>
                </div>
                <?php endif; ?>
                <div class="row">
                    <div class="col-md-12 text-center">
                        <a href="<?php echo esc_url( home_url( '/about-us/' ) ); ?>">
                            <button class="btn btn-quote text-uppercase">Learn More About Us</button>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php get_template_part('newsletter', 'signup');  ?>

<!-- TVC -->
<section>
    <div class="container-fluid pl-3 pr-3">
        <div class="row">
            <div class="col-md-12">
                <h2 class="text-uppercase text-center font-xbold">
                    <?php the_sub_field( 'tvc_title' ); ?>
                </h2>
                <h3 class="italic text-center">
                    <?php the_sub_field( 'tvc_slogan' ); ?>
                </h3>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="embed-container">
                    <?php the_sub_field( 'tvc_url_1' ); ?>
                </div>
            </div>
            <div class="col-md-6">
                <div class="embed-container">
                    <?php the_sub_field( 'tvc_url_2' ); ?>
                </div>
            </div>
        </div>
    </div>


   <!-- <script type="text/javascript"> 
        var user ="test";
        var pass ="pass";
       
       document.write(user + " " + pass);
    </script>  -->


</section>



<!-- /TVC -->
<!-- /Homepage -->

<!-- /#content -->