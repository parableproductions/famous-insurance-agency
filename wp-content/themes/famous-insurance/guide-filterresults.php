<?php
/**
 * The template for displaying search results pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package Famous_Insurance
 */

get_header(); ?>

<section class="no-padding-top">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <img src="https://famousinsurance.com.au/wp-content/uploads/2018/09/RequestAQuoteHero.jpg">
            </div>
        </div>
    </div>
</section>

<div class="container">
    <div class="row">
        <div class="col-md-12">
            <h2 class="text-uppercase text-center font-xbold">
                Autofill By <span class="text-red">Vehicle</span></h2>
            <h3 class="italic text-center">
                You can get your vehicle covered quickly and easily</h3>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="font-light">
                <p style="text-align: center;">Filter by vehicle make, model and year then select your vehicle to autofill your insurance quote.</p>
            </div>
        </div>
    </div>
</div>

<section>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <?php get_template_part( 'guide', 'filter' ); ?>
            </div>
        </div>
    </div>
</section>

<?php if( !empty( $_GET['model'] ) ) { ?>

<div class="container">
    <div class="row">
        <div class="col-md-12">
            <h2 class="text-uppercase text-center font-xbold">
                Select Your <span class="text-red">Vehicle</span></h2>
            <h3 class="italic text-center">
                Select your vehicle from the options below</h3>
        </div>
    </div>
</div>

<section id="content">
    <div class="container guidefilter-results font-light">

        <?php                 
			if( !empty( $_GET['make'] ) ) {
				$_make = $_GET['make'];
			}
    if( !empty( $_GET['model'] ) ) {
				$_model = $_GET['model'];
			}
    if( !empty( $_GET['year'] ) ) {
				$_year = $_GET['year'];
			}
        if( !empty( $_GET['type'] ) ) {
				$_type = $_GET['type'];
			}

			if (isset($_make))
			{
				$meta_query[] =  array(
					'key' => 'gg_make',
					'value' => $_make,
					'type' => 'text',
					'compare' => 'IN'
				);
			}
    
    if (isset($_model))
			{
				$meta_query[] =  array(
					'key' => 'gg_model',
					'value' => $_model,
					'type' => 'text',
					'compare' => 'IN'
				);
			}
    
    if (isset($_year))
			{
				$meta_query[] =  array(
					'key' => 'gg_year',
					'value' => $_year,
					'type' => 'text',
					'compare' => 'IN'
				);
			} ?>

        <!-- Results -->
        <?php global $wpdb;
$ggmakes = $wpdb->get_results("SELECT * FROM wp_glasses_guide WHERE  gg_year='$_year' AND gg_make='$_make' AND gg_model='$_model'"); ?>

        <?php foreach($ggmakes as $ggmake){ ?>

        <?php
    $make = $ggmake->gg_make;
    $model = $ggmake->gg_model;
    $year = $ggmake->gg_year;
    $engine = $ggmake->gg_engine;
        ?>

        <div class="row pt-1 pb-1">
            <div class="col-md-12">
                <a href="<?php echo get_site_url(); ?>/get-a-quote/<?php echo $_type; ?>/?make=<?php echo $make; ?>&model=<?php echo $model; ?>&make-year=<?php echo $year; ?>&engine=<?php echo $engine; ?>" target="_blank">
                    <?php echo $ggmake->gg_year; ?>
                    <?php echo $ggmake->gg_make; ?>
                    <?php echo $ggmake->gg_model; ?>
                    <?php echo $ggmake->gg_type; ?>,
                    <?php echo $ggmake->gg_series; ?>
                    <?php echo $ggmake->gg_cc; ?>
                    <?php echo $ggmake->gg_engine; ?>
                    <?php echo $ggmake->gg_transmission; ?>
                    <?php echo $ggmake->gg_size; ?>
                    <?php echo $ggmake->gg_cylinders; ?>
                    <?php echo $ggmake->gg_transmission; ?>
                </a>
            </div>
        </div>
        <?php } ?>
        <!-- /Results -->

    </div>
</section>

<?php } ?>

<?php get_footer(); ?>
