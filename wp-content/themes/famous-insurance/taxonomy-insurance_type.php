<?php
/**
 * The template for displaying archive pages.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Whats_Up_Downunder
 */
 $term = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) );
get_header(); ?>

    <?php 
    $image = get_field('top_section_bg', $term );
    if( !empty($image) ): 
    // vars
    $title = $image['title'];
    $alt = $image['alt'];

	// image
    $size = 'hero-image';
	$thumb = $image['sizes'][ $size ];
	$width = $image['sizes'][ $size . '-width' ];
	$height = $image['sizes'][ $size . '-height' ];
    ?>

    <section class="no-padding-top">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <img src="<?php echo $thumb; ?>" alt="<?php echo $alt; ?>" width="<?php echo $width; ?>" />
                </div>
            </div>
        </div>
    </section>
    <?php endif; ?>

    <section>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2 class="text-uppercase text-center font-xbold">
                        <?php single_term_title(); ?>
                    </h2>
                    <h3 class="italic text-center">
                        <?php the_field( 'insurance_type_slogan', $term ); ?>
                    </h3>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="font-light">
                        <?php the_field( 'insurance_type_description', $term ); ?>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- Red Section -->
    <section class="section-red">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2 class="text-uppercase text-center font-xbold">
                        <?php the_field( 'featured_section_title', $term ); ?>
                    </h2>
                    <h3 class="italic text-center">
                        <?php the_field( 'featured_section_slogan', $term ); ?>
                    </h3>
                    <hr/ class="hr-white">
                </div>
            </div>

            <?php if( have_rows('featured_section_repeater', $term) ): ?>
            <div class="row">
                <?php while( have_rows('featured_section_repeater', $term) ): the_row(); 
                // vars
                $title = get_sub_field('featured_section_single_title', $term);
                $content = get_sub_field('featured_section_single_content', $term);
                ?>
                <div class="col-md-4 text-center">
                    <p class="text-uppercase font-bold title">
                        <?php echo $title; ?>
                    </p>
                    <p class="font-light">
                        <?php echo $content; ?>
                    </p>
                </div>
                <?php endwhile; ?>
            </div>
            <?php endif; ?>
        </div>
    </section>
    <!-- /Red Section -->

    <section>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2 class="text-uppercase text-center font-xbold">
                        <?php the_field( 'insurance_type_list', $term ); ?>
                    </h2>
                    <h3 class="italic text-center">
                        <?php the_field( 'insurance_type_list_slogan', $term ); ?>
                    </h3>
                </div>
            </div>

            <?php
$args = array(
  'post_type'   => 'insurance',
  'post_status' => 'publish',
  'tax_query'   => array(
  	array(
  		'taxonomy' => 'insurance_type',
  		'field'    => 'slug',
  		'terms'    => $term
  	)
  )
 );
 
$range = new WP_Query( $args );
if( $range->have_posts() ) :
?>
                <div class="customer-logos">
                    <?php
      while( $range->have_posts() ) :
        $range->the_post();
        ?>
                        <div class="slide">
                            <?php $insurance_icon = get_field( 'insurance_icon' ); ?>
                            <?php if ( $insurance_icon ) { ?>
                            <a class="insurance-icon" href="<?php the_permalink(); ?>">
                            <img class="img-fluid pb-2" src="<?php echo $insurance_icon['url']; ?>" alt="<?php echo $insurance_icon['alt']; ?>" />
                            <?php } ?>
                            <p class="font-light text-center">
                                <?php the_title(); ?>
                            </p>
                            </a>
                        </div>
                        <?php endwhile; ?>

                </div>
                <?php endif; ?>

        </div>
    </section>

    <section>
        <div class="container-fluid pr-2">
            <div class="row">
                <div class="col-md-12">
                    <h2 class="text-uppercase text-center font-xbold">
                        <?php the_field( 'insurance_type_second_title', $term ); ?>
                    </h2>
                    <h3 class="italic text-center">
                        <?php the_field( 'insurance_type_second_slogan', $term ); ?>
                    </h3>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <?php 
                    $image = get_field('insurance_type_image', $term);
                    $width = get_field('insurnce_type_image_width', $term);
                    if( !empty($image) ): ?>
                    <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" width="<?php echo $width ?>%" />
                    <?php endif; ?>
                </div>
                <div class="col-md-6 justify-content-center align-self-center">

                    <?php if( have_rows('insurance_type_content_repeater', $term) ): ?>
                    <div class="row">
                        <?php while( have_rows('insurance_type_content_repeater', $term) ): the_row(); 
                // vars
                $title = get_sub_field('insurance_type_content_title', $term);
                $content = get_sub_field('insurance_type_content_description', $term);
                ?>
                        <div class="col-md-6 text-center">
                            <h4 class="text-uppercase text-center font-bold">
                                <?php echo $title; ?>
                            </h4>
                            <p class="font-light">
                                <?php echo $content; ?>
                            </p>
                        </div>
                        <?php endwhile; ?>
                    </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </section>

    <!-- Red Section -->
    <section class="section-red">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2 class="text-uppercase text-center font-xbold">
                        <?php the_field( 'steps_title', $term ); ?>
                    </h2>
                    <h3 class="italic text-center">
                        <?php the_field( 'steps_slogan', $term ); ?>
                    </h3>
                    <hr/ class="hr-white">
                </div>
            </div>

            <?php if( have_rows('steps_repeater', $term) ): ?>
            <div class="row">
                <?php while( have_rows('steps_repeater', $term) ): the_row(); 
                // vars
                $title = get_sub_field('steps_single_title', $term);
                $content = get_sub_field('steps_single_content', $term);
                ?>
                <div class="col-md-4 text-center">
                    <p class="text-uppercase font-bold title">
                        <?php echo $title; ?>
                    </p>
                    <p class="font-light">
                        <?php echo $content; ?>
                    </p>
                </div>
                <?php endwhile; ?>
            </div>
            <?php endif; ?>
        </div>
    </section>
    <!-- /Red Section -->

    <section class="pb-0">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2 class="text-uppercase text-center font-xbold">
                        <?php the_field( 'disclosure_statement_header', $term ); ?>
                    </h2>
                    <h3 class="italic text-center">
                        <?php the_field( 'disclosure_statement_slogan', $term ); ?>
                    </h3>
                    <!-- <p class="text-center"><a href="<?php echo esc_url( home_url( '/get-a-quote/' ) ); ?>" class="btn btn-quote text-uppercase">Request A Quote</a></p> -->
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">

                    <?php $attached_documents = get_field( 'attached_documents', $term ); ?>
                    <?php if ( $attached_documents ): ?>
                    <ul class="list-group list-group-flush">
                        <?php foreach ( $attached_documents as $post ):  ?>
                        <?php setup_postdata ( $post ); ?>
                        <li class="list-group-item">
                            <a class="text-red" href="<?php the_field('document_upload'); ?>" target="_blank">
                                <i class="fal fa-file-pdf"></i> <?php the_field('document_title'); ?>
                            </a>
                        </li>
                        <?php endforeach; ?>
                        <?php wp_reset_postdata(); ?>
                        <li class="list-group-item"><a class="text-red" href="<?php echo home_url(); ?>/documents/"><i class="fas fa-file-alt"></i> View All Policy Documents</a></li>
                    </ul>
                    <?php endif; ?>

                </div>
            </div>
        </div>
    </section>

    <?php get_footer(); ?>
