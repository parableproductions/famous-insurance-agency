<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Famous_Insurance
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>

<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="http://gmpg.org/xfn/11">

    <script src='https://www.google.com/recaptcha/api.js'></script>

   
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-132701698-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-132701698-1');
</script>

    <!-- End Global site tag (gtag.js) - Google Analytics -->

    <?php wp_head(); ?>



    <!-- Google Tag Manager -->
    <script>
    (function(w, d, s, l, i) {
        w[l] = w[l] || [];
        w[l].push({
            'gtm.start': new Date().getTime(),
            event: 'gtm.js'
        });
        var f = d.getElementsByTagName(s)[0],
            j = d.createElement(s),
            dl = l != 'dataLayer' ? '&l=' + l : '';
        j.async = true;
        j.src =
            'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
        f.parentNode.insertBefore(j, f);
    })(window, document, 'script', 'dataLayer', 'GTM-PQFHPZQ');
    </script>
    <!-- End Google Tag Manager -->

    


    <!-- Facebook Pixel Code -->
    <script>
    ! function(f, b, e, v, n, t, s) {
        if (f.fbq) return;
        n = f.fbq = function() {
            n.callMethod ?
                n.callMethod.apply(n, arguments) : n.queue.push(arguments)
        };
        if (!f._fbq) f._fbq = n;
        n.push = n;
        n.loaded = !0;
        n.version = '2.0';
        n.queue = [];
        t = b.createElement(e);
        t.async = !0;
        t.src = v;
        s = b.getElementsByTagName(e)[0];
        s.parentNode.insertBefore(t, s)
    }(window, document, 'script',
        'https://connect.facebook.net/en_US/fbevents.js');
    fbq('init', '335709754017719');
    fbq('track', 'PageView');
    </script>
    <noscript><img height="1" width="1" style="display:none"
            src="https://www.facebook.com/tr?id=335709754017719&ev=PageView&noscript=1" /></noscript>
    <!-- End Facebook Pixel Code -->

   </head>

<body <?php body_class(); ?>>

    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-PQFHPZQ" height="0" width="0"
            style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->



    <script language='JavaScript1.1' async
        src='//pixel.mathtag.com/event/js?mt_id=1380926&mt_adid=218974&mt_exem=&mt_excl='></script>

    <section>
        <div class="container">
            <div class="row align-items-center">
                <div class="col-md-4">
                    <img class="img-fluid img-logo"
                        src="<?php bloginfo('stylesheet_directory'); ?>/assets/img/famous-logo-chrome.png"
                        alt="Famous Insurance">
                </div>
                <!-- Social Icons -->
                <div class="col-md-5"></div>
                <!-- Request Quote -->
                <!-- <div class="col-md-3 d-none d-md-block">
                    <a class="d-flex h-100" href="<?php echo esc_url( home_url( '/get-a-quote/' ) ); ?>">
                        <button class="btn btn-block btn-quote request-quote justify-content-center">Request A
                            Quote</button>
                    </a>
                </div> -->
                <!-- /Request Quote -->
                <!-- Enquiries -->
                <div class="col-md-3 enquiries text-right font-semi" style="font-size:0.8rem">
                    <span>
                        For enquiries call us at<br>
                    </span>
                    <span class="text-red text-uppercase" style="font-size:1.5rem">
                        <a href="tel:1300-32-66-87">1300 Famous</a><br>
                    </span>
                    <span style="font-size:1rem">
                        <a href="tel:1300-32-66-87">1300 32 66 87</a>
                    </span>
                </div>
                <!-- /Enquiries -->
            </div>
        </div>
    </section>

    <section class="mainnav">


        <div class="container-fluid pl-2 pr-2">
                        <!--  -->
                        <a class="menu-quote" href="<?php echo esc_url( home_url( '/get-a-quote/' ) ); ?>">
                        Get A Quote 
            </a>
            <!--  -->
            <?php
   wp_nav_menu([
     'theme_location'  => 'menu-1',
     'container'       => 'div',
     'container_id'    => 'bs4navbar',
     'container_class' => 'collapse navbar-collapse',
     'menu_id'         => false,
     'menu_class'      => 'navbar-nav mr-auto',
     'depth'           => 2,
     'fallback_cb'     => 'bs4navwalker::fallback'
   ]);
   ?>

        </div>
    </section>