<?php defined( 'WPINC' ) or die('Nothing to see here.'); return array (
  'tables' => 
  array (
    'glasses_guide' => 
    array (
      'name' => 'glasses_guide',
      'relationship' => 
      array (
        'type' => 'post',
        'post_type' => 'glasses_guide',
      ),
      'primary_key' => 
      array (
        0 => 'id',
      ),
      'keys' => 
      array (
        0 => 
        array (
          'name' => 'post_id',
          'columns' => 
          array (
            0 => 'post_id',
          ),
          'unique' => true,
        ),
      ),
      'columns' => 
      array (
        0 => 
        array (
          'name' => 'id',
          'format' => '%d',
          'null' => false,
          'auto_increment' => true,
          'unsigned' => true,
          'map' => 
          array (
            'type' => 'acf_field_name',
            'identifier' => 'id',
          ),
        ),
        1 => 
        array (
          'name' => 'post_id',
          'format' => '%d',
          'map' => 
          array (
            'type' => 'acf_field_name',
            'identifier' => 'post_id',
          ),
        ),
        2 => 
        array (
          'name' => 'gg_index',
          'map' => 
          array (
            'type' => 'acf_field_name',
            'identifier' => 'gg_index',
          ),
          'format' => '%s',
        ),
        3 => 
        array (
          'name' => 'gg_year',
          'map' => 
          array (
            'type' => 'acf_field_name',
            'identifier' => 'gg_year',
          ),
          'format' => '%s',
        ),
        4 => 
        array (
          'name' => 'gg_make',
          'map' => 
          array (
            'type' => 'acf_field_name',
            'identifier' => 'gg_make',
          ),
          'format' => '%s',
        ),
        5 => 
        array (
          'name' => 'gg_model',
          'map' => 
          array (
            'type' => 'acf_field_name',
            'identifier' => 'gg_model',
          ),
          'format' => '%s',
        ),
        6 => 
        array (
          'name' => 'gg_variant',
          'map' => 
          array (
            'type' => 'acf_field_name',
            'identifier' => 'gg_variant',
          ),
          'format' => '%s',
        ),
        7 => 
        array (
          'name' => 'gg_series',
          'map' => 
          array (
            'type' => 'acf_field_name',
            'identifier' => 'gg_series',
          ),
          'format' => '%s',
        ),
        8 => 
        array (
          'name' => 'gg_style',
          'map' => 
          array (
            'type' => 'acf_field_name',
            'identifier' => 'gg_style',
          ),
          'format' => '%s',
        ),
        9 => 
        array (
          'name' => 'gg_engine',
          'map' => 
          array (
            'type' => 'acf_field_name',
            'identifier' => 'gg_engine',
          ),
          'format' => '%s',
        ),
        10 => 
        array (
          'name' => 'gg_transmission',
          'map' => 
          array (
            'type' => 'acf_field_name',
            'identifier' => 'gg_transmission',
          ),
          'format' => '%s',
        ),
        11 => 
        array (
          'name' => 'gg_cc',
          'map' => 
          array (
            'type' => 'acf_field_name',
            'identifier' => 'gg_cc',
          ),
          'format' => '%s',
        ),
        12 => 
        array (
          'name' => 'gg_size',
          'map' => 
          array (
            'type' => 'acf_field_name',
            'identifier' => 'gg_size',
          ),
          'format' => '%s',
        ),
        13 => 
        array (
          'name' => 'gg_cylinders',
          'map' => 
          array (
            'type' => 'acf_field_name',
            'identifier' => 'gg_cylinders',
          ),
          'format' => '%s',
        ),
        14 => 
        array (
          'name' => 'gg_drivenwheels',
          'map' => 
          array (
            'type' => 'acf_field_name',
            'identifier' => 'gg_drivenwheels',
          ),
          'format' => '%s',
        ),
        15 => 
        array (
          'name' => 'gg_pricelow',
          'map' => 
          array (
            'type' => 'acf_field_name',
            'identifier' => 'gg_pricelow',
          ),
          'format' => '%s',
        ),
        16 => 
        array (
          'name' => 'gg_pricemid',
          'map' => 
          array (
            'type' => 'acf_field_name',
            'identifier' => 'gg_pricemid',
          ),
          'format' => '%s',
        ),
        17 => 
        array (
          'name' => 'gg_pricehigh',
          'map' => 
          array (
            'type' => 'acf_field_name',
            'identifier' => 'gg_pricehigh',
          ),
          'format' => '%s',
        ),
        18 => 
        array (
          'name' => 'gg_newprice',
          'map' => 
          array (
            'type' => 'acf_field_name',
            'identifier' => 'gg_newprice',
          ),
          'format' => '%s',
        ),
        19 => 
        array (
          'name' => 'gg_torque',
          'map' => 
          array (
            'type' => 'acf_field_name',
            'identifier' => 'gg_torque',
          ),
          'format' => '%s',
        ),
        20 => 
        array (
          'name' => 'gg_powerkw',
          'map' => 
          array (
            'type' => 'acf_field_name',
            'identifier' => 'gg_powerkw',
          ),
          'format' => '%s',
        ),
        21 => 
        array (
          'name' => 'gg_kerbweight',
          'map' => 
          array (
            'type' => 'acf_field_name',
            'identifier' => 'gg_kerbweight',
          ),
          'format' => '%s',
        ),
        22 => 
        array (
          'name' => 'gg_fueltype',
          'map' => 
          array (
            'type' => 'acf_field_name',
            'identifier' => 'gg_fueltype',
          ),
          'format' => '%s',
        ),
        23 => 
        array (
          'name' => 'gg_segment',
          'map' => 
          array (
            'type' => 'acf_field_name',
            'identifier' => 'gg_segment',
          ),
          'format' => '%s',
        ),
        24 => 
        array (
          'name' => 'gg_bav',
          'map' => 
          array (
            'type' => 'acf_field_name',
            'identifier' => 'gg_bav',
          ),
          'format' => '%s',
        ),
        25 => 
        array (
          'name' => 'gg_av',
          'map' => 
          array (
            'type' => 'acf_field_name',
            'identifier' => 'gg_av',
          ),
          'format' => '%s',
        ),
        26 => 
        array (
          'name' => 'gg_aav',
          'map' => 
          array (
            'type' => 'acf_field_name',
            'identifier' => 'gg_aav',
          ),
          'format' => '%s',
        ),
        27 => 
        array (
          'name' => 'gg_avnew',
          'map' => 
          array (
            'type' => 'acf_field_name',
            'identifier' => 'gg_avnew',
          ),
          'format' => '%s',
        ),
        28 => 
        array (
          'name' => 'gg_segment_grouped',
          'map' => 
          array (
            'type' => 'acf_field_name',
            'identifier' => 'gg_segment_grouped',
          ),
          'format' => '%s',
        ),
        29 => 
        array (
          'name' => 'gg_power_to_weight',
          'map' => 
          array (
            'type' => 'acf_field_name',
            'identifier' => 'gg_power_to_weight',
          ),
          'format' => '%s',
        ),
        30 => 
        array (
          'name' => 'gg_vehicle_make_group',
          'map' => 
          array (
            'type' => 'acf_field_name',
            'identifier' => 'gg_vehicle_make_group',
          ),
          'format' => '%s',
        ),
        31 => 
        array (
          'name' => 'gg_uw_rule_1',
          'map' => 
          array (
            'type' => 'acf_field_name',
            'identifier' => 'gg_uw_rule_1',
          ),
          'format' => '%s',
        ),
        32 => 
        array (
          'name' => 'gg_uw_rule_2',
          'map' => 
          array (
            'type' => 'acf_field_name',
            'identifier' => 'gg_uw_rule_2',
          ),
          'format' => '%s',
        ),
      ),
      'hash' => '29e9a19ddcea2c376499dcdc3cc29226',
      'modified' => 1556767883,
      'type' => 'meta',
    ),
  ),
  'table_names' => 
  array (
    0 => 'glasses_guide',
  ),
  'types' => 
  array (
    'post' => 
    array (
      0 => 0,
    ),
  ),
  'post_types' => 
  array (
    'glasses_guide' => 
    array (
      0 => 0,
    ),
  ),
  'join_tables' => 
  array (
  ),
  'meta_tables' => 
  array (
  ),
  'acf_field_names' => 
  array (
    'post:glasses_guide' => 
    array (
      'id' => 
      array (
        0 => 0,
      ),
      'post_id' => 
      array (
        0 => 0,
      ),
      'gg_index' => 
      array (
        0 => 0,
      ),
      'gg_year' => 
      array (
        0 => 0,
      ),
      'gg_make' => 
      array (
        0 => 0,
      ),
      'gg_model' => 
      array (
        0 => 0,
      ),
      'gg_variant' => 
      array (
        0 => 0,
      ),
      'gg_series' => 
      array (
        0 => 0,
      ),
      'gg_style' => 
      array (
        0 => 0,
      ),
      'gg_engine' => 
      array (
        0 => 0,
      ),
      'gg_transmission' => 
      array (
        0 => 0,
      ),
      'gg_cc' => 
      array (
        0 => 0,
      ),
      'gg_size' => 
      array (
        0 => 0,
      ),
      'gg_cylinders' => 
      array (
        0 => 0,
      ),
      'gg_drivenwheels' => 
      array (
        0 => 0,
      ),
      'gg_pricelow' => 
      array (
        0 => 0,
      ),
      'gg_pricemid' => 
      array (
        0 => 0,
      ),
      'gg_pricehigh' => 
      array (
        0 => 0,
      ),
      'gg_newprice' => 
      array (
        0 => 0,
      ),
      'gg_torque' => 
      array (
        0 => 0,
      ),
      'gg_powerkw' => 
      array (
        0 => 0,
      ),
      'gg_kerbweight' => 
      array (
        0 => 0,
      ),
      'gg_fueltype' => 
      array (
        0 => 0,
      ),
      'gg_segment' => 
      array (
        0 => 0,
      ),
      'gg_bav' => 
      array (
        0 => 0,
      ),
      'gg_av' => 
      array (
        0 => 0,
      ),
      'gg_aav' => 
      array (
        0 => 0,
      ),
      'gg_avnew' => 
      array (
        0 => 0,
      ),
      'gg_segment_grouped' => 
      array (
        0 => 0,
      ),
      'gg_power_to_weight' => 
      array (
        0 => 0,
      ),
      'gg_vehicle_make_group' => 
      array (
        0 => 0,
      ),
      'gg_uw_rule_1' => 
      array (
        0 => 0,
      ),
      'gg_uw_rule_2' => 
      array (
        0 => 0,
      ),
    ),
  ),
  'acf_field_column_names' => 
  array (
  ),
);