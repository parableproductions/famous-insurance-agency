<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Famous_Insurance
 */

?>
<section class="pt-3 pb-3">
    <div class="container pt-3 pb-3">

        <div class="row">
            <!-- Content -->
            <div class="col-md-9">
                <!-- Description -->

                <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                    <header class="entry-header">
                        <?php
		if ( is_singular() ) :
			the_title( '<h3 class="entry-title text-center">', '</h3>' );
		else :
			the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
		endif;

		if ( 'post' === get_post_type() ) :
			?>
                        <div class="entry-meta font-light">
                            <?php
				famous_insurance_posted_on();
				?>
                        </div><!-- .entry-meta -->
                        <?php endif; ?>
                    </header><!-- .entry-header -->
                    <div class="entry-content">
                        <?php
		the_content( sprintf(
			wp_kses(
				/* translators: %s: Name of current post. Only visible to screen readers */
				__( 'Continue reading<span class="screen-reader-text"> "%s"</span>', 'famous-insurance' ),
				array(
					'span' => array(
						'class' => array(),
					),
				)
			),
			get_the_title()		) );
wp_link_pages( array(
'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'famous-insurance' ),
    'after' => '</div>',
) );
		?>

                    </div><!-- .entry-content -->
                </article><!-- #post-<?php the_ID(); ?> -->
                <!-- /Content -->
            </div>
            <!-- Sidebar -->
            <div class="col-md-3">
                <?php get_sidebar(); ?>
            </div>
        </div>
    </div>
    <!-- Sidebar -->
</section>
