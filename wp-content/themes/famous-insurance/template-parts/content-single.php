<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Famous_Insurance
 */

?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    <div class="card">
        <?php if ( has_post_thumbnail()) : ?>
        <?php the_post_thumbnail( 'large', array( 'class' => 'img-fluid' ) ); ?>
        <?php endif; ?>

        <div class="card-body">
            <div class="entry-meta font-light">
                <h5><?php the_title(); ?></h5>
                <?php famous_insurance_posted_on(); ?>
            </div>
            <br>
            <?php the_excerpt(); ?>

            <a href="<?php the_permalink(); ?>" class="btn btn-quote">Read More</a>
        </div>
    </div>
</article>
