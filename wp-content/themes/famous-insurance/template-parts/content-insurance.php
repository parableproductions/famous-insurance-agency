<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Famous_Insurance
 */

get_header(); ?>

    <?php 
    $image = get_field('top_section_bg' );
    if( !empty($image) ): 
    // vars
    $title = $image['title'];
    $alt = $image['alt'];

	// image
    $size = 'hero-image';
	$thumb = $image['sizes'][ $size ];
	$width = $image['sizes'][ $size . '-width' ];
	$height = $image['sizes'][ $size . '-height' ];
    ?>

    <section class="no-padding-top">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <img src="<?php echo $thumb; ?>" alt="<?php echo $alt; ?>" width="<?php echo $width; ?>" />
                </div>
            </div>
        </div>
    </section>
    <?php endif; ?>

    <section>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2 class="text-uppercase text-center font-xbold">
                        <?php the_field( 'insurance_type_title' ); ?>
                    </h2>
                    <h3 class="italic text-center">
                        <?php the_field( 'insurance_type_slogan' ); ?>
                    </h3>
                    <hr/ class="hr-white">
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <?php the_field('insurance_type_description'); ?>
                </div>
            </div>
        </div>
    </section>

    <?php $custom_taxterms = wp_get_object_terms( $post->ID, 'insurance_type', array('fields' => 'ids') );
// arguments
$args = array(
'post_type' => 'insurance',
'post_status' => 'publish',
'tax_query' => array(
    array(
        'taxonomy' => 'insurance_type',
        'field' => 'id',
        'terms' => $custom_taxterms
    )
), );
$related_items = new WP_Query( $args );
// loop over query
if ($related_items->have_posts()) : ?>


    <section>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2 class="text-uppercase text-center font-xbold">
                        <?php the_field( 'insurance_type_list' ); ?>
                    </h2>
                    <h3 class="italic text-center">
                        <?php the_field( 'insurance_type_list_slogan' ); ?>
                    </h3>
                </div>
            </div>

            <div class="customer-logos">
                <?php while ( $related_items->have_posts() ) : $related_items->the_post(); ?>
                <div class="slide">
                    <?php $insurance_icon = get_field( 'insurance_icon' ); ?>
                    <?php if ( $insurance_icon ) { ?>
                    <a class="insurance-icon" href="<?php the_permalink(); ?>">
                            <img class="img-fluid pb-2" src="<?php echo $insurance_icon['url']; ?>" alt="<?php echo $insurance_icon['alt']; ?>" />
                            <?php } ?>
                            <p class="font-light text-center">
                                <?php the_title(); ?>
                            </p>
                            </a>
                </div>
                <?php endwhile; ?>
            </div>
        </div>
    </section>
    <?php endif;
wp_reset_postdata(); ?>

    <!-- Red Section -->
    <?php if( get_field('featured_section_title') ): ?>
    <section class="section-red">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2 class="text-uppercase text-center font-xbold">
                        <?php the_field( 'featured_section_title' ); ?>
                    </h2>
                    <h3 class="italic text-center">
                        <?php the_field( 'featured_section_slogan' ); ?>
                    </h3>
                    <hr/ class="hr-white">
                </div>
            </div>

            <?php if( have_rows('featured_section_repeater') ): ?>
            <div class="row">
                <?php while( have_rows('featured_section_repeater') ): the_row(); 
                // vars
                $title = get_sub_field('featured_section_single_title');
                $content = get_sub_field('featured_section_single_content');
                ?>
                <div class="col-md-4 text-center">
                    <p class="text-uppercase font-bold title">
                        <?php echo $title; ?>
                    </p>
                    <p class="font-light">
                        <?php echo $content; ?>
                    </p>
                </div>
                <?php endwhile; ?>
            </div>
            <?php endif; ?>
        </div>
    </section>
    <?php endif; ?>
    <!-- /Red Section -->

    <section>
        <div class="container-fluid pt-3">
            <div class="row">
                <div class="col-md-12">
                    <h2 class="text-uppercase text-center font-xbold">
                        <?php the_field( 'insurance_type_second_title' ); ?>
                    </h2>
                    <h3 class="italic text-center pb-3">
                        <?php the_field( 'insurance_type_second_slogan' ); ?>
                    </h3>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-6 col-md-12">
                    <?php 
                    $image = get_field('insurance_type_image');
                    $width = get_field('insurnce_type_image_width');
                    if( !empty($image) ): ?>
                    <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" width="<?php echo $width ?>%" />
                    <?php endif; ?>
                </div>
                <div class="col-lg-6 col-md-12 justify-content-center align-self-center pl-5 pb-2 pr-5 pt-2">

                    <?php if( have_rows('insurance_type_content_repeater') ): ?>
                    <div class="row">
                        <?php while( have_rows('insurance_type_content_repeater') ): the_row(); 
                // vars
                $title = get_sub_field('insurance_type_content_title');
                $content = get_sub_field('insurance_type_content_description');
                ?>
                        <div class="col-md-6 text-center">
                            <h4 class="text-uppercase text-center font-bold">
                                <?php echo $title; ?>
                            </h4>
                            <p class="font-light">
                                <?php echo $content; ?>
                            </p>
                        </div>
                        <?php endwhile; ?>
                    </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </section>

    <?php if( get_field('whats_covered') ): ?>
    <section>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2 class="text-uppercase text-center font-xbold">What's Covered</h2>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="font-light">
                        <?php the_field( 'whats_covered' ); ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <?php endif; ?>

    <!-- Red Section -->
    <?php if( get_field('steps_section_title') ): ?>
    <section class="section-red">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2 class="text-uppercase text-center font-xbold">
                        <?php the_field( 'steps_section_title' ); ?>
                    </h2>
                    <h3 class="italic text-center">
                        <?php the_field( 'steps_section_slogan' ); ?>
                    </h3>
                    <hr/ class="hr-white">
                </div>
            </div>

            <?php if( have_rows('steps_section_repeater') ): ?>
            <div class="row">
                <?php while( have_rows('steps_section_repeater') ): the_row(); 
                // vars
                $title = get_sub_field('steps_section_single_title');
                $content = get_sub_field('steps_section_single_content');
                ?>
                <div class="col-md-4 text-center">
                    <p class="text-uppercase font-bold title">
                        <?php echo $title; ?>
                    </p>
                    <p class="font-light">
                        <?php echo $content; ?>
                    </p>
                </div>
                <?php endwhile; ?>
            </div>
            <?php endif; ?>
        </div>
    </section>
    <?php endif; ?>
    <!-- /Red Section -->

    <section class="pb-0">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2 class="text-uppercase text-center font-xbold">
                        <?php the_field( 'disclosure_statement_header', $term ); ?>
                    </h2>
                    <h3 class="italic text-center">
                        <?php the_field( 'disclosure_statement_slogan', $term ); ?>
                    </h3>
                    <!-- <p class="text-center"><a href="<?php the_field('quote_form_url'); ?>" class="btn btn-quote request-quote text-uppercase">Request A Quote</a></p> -->
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <?php $attached_documents = get_field( 'attached_documents', $term ); ?>
                    <?php if ( $attached_documents ): ?>
                    <ul class="list-group list-group-flush">
                        <?php foreach ( $attached_documents as $post ):  ?>
                        <?php setup_postdata ( $post ); ?>
                        <li class="list-group-item">
                            <a class="text-red" href="<?php the_field('document_upload'); ?>" target="_blank">
                                <i class="fal fa-file-pdf"></i> <?php the_field('document_title'); ?>
                            </a>
                        </li>
                        <?php endforeach; ?>
                        <?php wp_reset_postdata(); ?>
                        <li class="list-group-item"><a class="text-red" href="<?php echo home_url(); ?>/documents/"><i class="fas fa-file-alt"></i> View All Policy Documents</a></li>
                    </ul>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </section>
