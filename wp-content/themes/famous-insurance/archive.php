<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Famous_Insurance
 */

get_header();
?>

<section class="pt-3 pb-3">
    <div class="row text-center">
        <div class="col-md-12">
            <h2 class="text-uppercase font-xbold">Famous<span class="text-red"> Last Words</span></h2>
        </div>
    </div>
    <div class="container pt-3 pb-3">

        <div class="row">
            <!-- Content -->
            <div class="col-md-9">

                <?php 
the_archive_description( '<div class="taxonomy-description">', '</div>' ); 
?>
                <div id="primary" class="content-area">
                    <main id="main" class="site-main">


                        <?php
		if ( have_posts() ) :

			if ( is_home() && ! is_front_page() ) :
				?>
                        <header>
                            <h1 class="page-title screen-reader-text"><?php single_post_title(); ?></h1>
                        </header>
                        <?php
			endif;

			/* Start the Loop */
			while ( have_posts() ) :
				the_post();

				/*
				 * Include the Post-Type-specific template for the content.
				 * If you want to override this in a child theme, then include a file
				 * called content-___.php (where ___ is the Post Type name) and that will be used instead.
				 */
get_template_part( 'template-parts/content', 'single' );
			endwhile;

			the_posts_navigation();

		else :

			get_template_part( 'template-parts/content', 'none' );

		endif;
		?>
                    </main><!-- #main -->
                </div><!-- #primary -->
            </div>
            <!-- Sidebar -->
            <div class="col-md-3">
                <?php get_sidebar(); ?>
            </div>
            <!-- Sidebar -->
        </div>
    </div>
</section>

<?php
get_footer();
