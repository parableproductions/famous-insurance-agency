<?php 
if( !empty( $_GET['make'] ) ) {
$search_make = $_GET['make'];
}
if( !empty( $_GET['model'] ) ) {
$search_model = $_GET['model'];
}
if( !empty( $_GET['year'] ) ) {
$search_year = $_GET['year'];
}
if( !empty( $_GET['type'] ) ) {
$vehicle_type = $_GET['type'];
}

if (isset($search_make))
{
$meta_query[] =  array(
'key' => 'gg_make',
'value' => $search_make,
'type' => 'text',
'compare' => 'IN'
);
}

if (isset($search_model))
{
$meta_query[] =  array(
'key' => 'gg_model',
'value' => $search_model,
'type' => 'text',
'compare' => 'IN'
);
}

if (isset($search_year))
{
$meta_query[] =  array(
'key' => 'gg_year',
'value' => $search_year,
'type' => 'text',
'compare' => 'IN'
);
}
?>

<?php global $wpdb;
$ggyears = $wpdb->get_results("SELECT gg_year FROM wp_glasses_guide ORDER BY gg_year DESC"); ?>

<?php foreach($ggyears as $ggyear){ ?>

<?php $years[] = $ggyear->gg_year; ?>

<?php
        $years = array_unique($years);
        $years = array_filter($years);
        $years = array_values($years);
        ?>

<?php } ?>

<?php if (isset($search_year)) { ?>

<?php global $wpdb;
$ggmakes = $wpdb->get_results("SELECT gg_make FROM wp_glasses_guide WHERE gg_year='$search_year' ORDER BY gg_year ASC");  ?>

<?php foreach($ggmakes as $ggmake){ ?>

<?php $makes[] = $ggmake->gg_make; ?>

<?php
        $makes = array_unique($makes);
        $makes = array_filter($makes);
        $makes = array_values($makes);
        ?>

<?php } ?>

<?php } ?>

<?php if (isset($search_make)) { ?>

<?php global $wpdb;
$ggmodels = $wpdb->get_results("SELECT gg_model FROM wp_glasses_guide WHERE gg_year='$search_year' AND gg_make='$search_make' ORDER BY gg_model ASC"); ?>

<?php foreach($ggmodels as $ggmodel){ ?>

<?php $models[] = $ggmodel->gg_model; ?>

<?php
        $models = array_unique($models);
        $models = array_filter($models);
        $models = array_values($models);
        ?>

<?php } ?>

<?php foreach($ggvariants as $ggvariant){ ?>

<?php $variants[] = $ggvariant->gg_variant; ?>

<?php
        $variants = array_unique($variants);
        $variants = array_filter($variants);
        $variants = array_values($variants);
        ?>

<?php } ?>

<?php foreach($ggseries as $ggseriespost){ ?>

<?php $series[] = $ggseriespost->gg_series; ?>

<?php
        $series = array_unique($series);
        $series = array_filter($series);
        $series = array_values($series);
        ?>

<?php } ?>

<?php } ?>

<form role="search" method="get" class="search-form" id="guidefilter" action="<?php echo home_url( '/' ); ?>">

    <div class="row pt-2">

    <div class="col-md-4">
            <select name="year" class="form-control" id="year" onchange="this.form.submit()">
                <option value="" disabled selected hidden>
                    All Years
                </option>
                <?php foreach ( $years as $year ) { ?>
                <option value="<?php echo $year; ?>" <?php if ($year == $search_year) { ?>selected="selected" <?php } ?>><?php echo $year; ?></option>
                <?php } ?>
            </select>
        </div>

        <div class="col-md-4">
            <select name="make" class="form-control" id="make" <?php if( empty( $_GET['year'] ) ) { ?>disabled="disabled" <?php } ?> onchange="this.form.submit()">
                <option value="" disabled selected hidden>
                    All Makes
                </option>
                <?php foreach ( $makes as $make ) { ?>
                <option value="<?php echo $make; ?>" <?php if ($make == $search_make) { ?>selected="selected" <?php } ?>><?php echo $make; ?></option>
                <?php } ?>
            </select>
        </div>

        <div class="col-md-4">
            <select name="model" class="form-control" id="model" <?php if( empty( $_GET['year'] ) ) { ?>disabled="disabled" <?php } ?> onchange="this.form.submit()">
                <option value="" disabled selected hidden>
                    All Models
                </option>
                <?php foreach ( $models as $model ) { ?>
                <option value="<?php echo $model; ?>" <?php if ($model == $search_model) { ?>selected="selected" <?php } ?>><?php echo $model; ?></option>
                <?php } ?>
            </select>
        </div>

    </div>

    <input type="hidden" class="form-control keywords" name="s" id="search" placeholder="Enter Keywords..." value="<?php get_search_query(); ?>">

    <input type="hidden" name="type" value="<?php echo $vehicle_type; ?>" />

    <input type="hidden" name="search-type" value="guidefilter" />

</form>
