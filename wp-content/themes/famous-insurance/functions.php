<?php
/**
 * Famous Insurance functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Famous_Insurance
 */

session_start();

if ( ! function_exists( 'famous_insurance_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function famous_insurance_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on Famous Insurance, use a find and replace
		 * to change 'famous-insurance' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'famous-insurance', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );
        add_image_size( 'hero-image', 1920, 443 );

        // Include custom navwalker
        require_once('bs4navwalker.php');

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
			'menu-1' => esc_html__( 'Primary', 'famous-insurance' ),
            'menu-2' => esc_html__( 'Secondary', 'famous-insurance' ),
		) );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		// Set up the WordPress core custom background feature.
		add_theme_support( 'custom-background', apply_filters( 'famous_insurance_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		) ) );

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support( 'custom-logo', array(
			'height'      => 250,
			'width'       => 250,
			'flex-width'  => true,
			'flex-height' => true,
		) );
	}
endif;
add_action( 'after_setup_theme', 'famous_insurance_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function famous_insurance_content_width() {
	// This variable is intended to be overruled from themes.
	// Open WPCS issue: {@link https://github.com/WordPress-Coding-Standards/WordPress-Coding-Standards/issues/1043}.
	// phpcs:ignore WordPress.NamingConventions.PrefixAllGlobals.NonPrefixedVariableFound
	$GLOBALS['content_width'] = apply_filters( 'famous_insurance_content_width', 640 );
}
add_action( 'after_setup_theme', 'famous_insurance_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function famous_insurance_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'famous-insurance' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'famous-insurance' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );
}
add_action( 'widgets_init', 'famous_insurance_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function famous_insurance_scripts() {

	//deregister outdated scripts
	wp_deregister_script('jquery');
	wp_deregister_script('jquery-ui-core');

	//deregister unused/unwanted scripts
	wp_deregister_script('parsley');
	wp_deregister_style("parsley-stype-css");
	wp_deregister_style("jetpack_css-css");
	wp_deregister_style("jetpack-widget-social-icons-styles-css");
	wp_deregister_style("buttons-css");
	wp_deregister_style("parsley-stype-css");

	//reenable outdated scripts with updated versions
	wp_enqueue_script( 'jquery', 'https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js', array(), '3.3.1', true );
	wp_enqueue_script( 'jquery-ui', "https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js", array('jquery'), '1.12.1', false );
    wp_enqueue_style( 'jquery-ui-style', 'https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css');

    wp_enqueue_style( 'famous-insurance-bootstrap', get_template_directory_uri() . '/assets/css/bootstrap.min.css', array('jquery-ui-style'), "4.x");
    wp_enqueue_style( 'famous-insurance-montserrat', 'https://fonts.googleapis.com/css?family=Montserrat:300,400,600,800', array('famous-insurance-bootstrap'));
    wp_enqueue_style( 'famous-insurance-italianno', 'https://fonts.googleapis.com/css?family=Italianno', array('famous-insurance-montserrat'));

    wp_enqueue_style( 'famous-insurance-customstyle', get_template_directory_uri() . '/assets/css/custom.css', array('famous-insurance-italianno'));
    wp_enqueue_style( 'famous-insurance-allstyle', get_template_directory_uri() . '/assets/css/all.css', array('famous-insurance-customstyle'));

	wp_enqueue_style( 'famous-insurance-style', get_stylesheet_uri(), array("famous-insurance-allstyle") );

	wp_enqueue_script( 'famous-insurance-navigation', get_template_directory_uri() . '/js/navigation.js', array('jquery-ui'), '20151215', true );
	wp_enqueue_script( 'famous-insurance-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array("famous-insurance-navigation"), '20151215', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}

	wp_enqueue_script( 'famous-insurance-bootstrap-js', get_template_directory_uri() . '/assets/js/bootstrap.min.js', array('famous-insurance-skip-link-focus-fix'), '20151215', true );
	wp_enqueue_script( 'famous-insurance-popper-js', 'https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js', array('famous-insurance-bootstrap-js'), '4.x', true );
	wp_enqueue_script( 'famous-insurance-fontawesom-js', get_template_directory_uri() . '/assets/js/all.js', array('famous-insurance-bootstrap-js'), '4.x', true );
	wp_enqueue_script( 'famous-insurance-slick-carosel-js', 'https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.js', array('famous-insurance-bootstrap-js'), '4.x', true );
	wp_enqueue_script( 'famous-insurance-carosel-js', get_template_directory_uri() . '/assets/js/carousel.js', array('famous-insurance-bootstrap-js'), '4.x', true );

}
add_action( 'wp_enqueue_scripts', 'famous_insurance_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}

add_filter('acf_the_content', 'eae_encode_emails');

/* Categories */
add_action( 'init', 'create_insurance_type_hierarchical_taxonomy', 0 );

function create_insurance_type_hierarchical_taxonomy() {

  $labels = array(
    'name' => _x( 'Insurance Type', 'taxonomy general name' ),
    'singular_name' => _x( 'Insurance Type', 'taxonomy singular name' ),
    'search_items' =>  __( 'Search Insurance Types' ),
    'all_items' => __( 'All Insurance Types' ),
    'parent_item' => __( 'Parent Insurance Type' ),
    'parent_item_colon' => __( 'Parent Insurance Type:' ),
    'edit_item' => __( 'Edit Insurance Type' ),
    'update_item' => __( 'Update Insurance Type' ),
    'add_new_item' => __( 'Add New Insurance Type' ),
    'new_item_name' => __( 'New Insurance Type Name' ),
    'menu_name' => __( 'Insurance Types' ),
  );

  register_taxonomy('insurance_type',array('insurance','documents'), array(
    'hierarchical' => true,
    'labels' => $labels,
    'show_ui' => true,
    'show_admin_column' => true,
    'query_var' => true,
    'rewrite' => array( 'slug' => 'type' ),
  ));

}

add_filter( 'wpcf7_support_html5_fallback', '__return_true' );

/*
 * Disables storing of meta data values in core meta tables where a custom
 * database table has been defined for fields. Any fields that aren't mapped
 * to a custom database table will still be stored in the core meta tables.
 */
add_filter( 'acfcdt/settings/store_acf_values_in_core_meta', '__return_false' );

/*
 * Disables storing of ACF field key references in core meta tables where a custom
 * database table has been defined for fields. Any fields that aren't mapped to a
 * custom database table will still have their key references stored in the core
 * meta tables.
 */
add_filter( 'acfcdt/settings/store_acf_keys_in_core_meta', '__return_false' );


// Ajax

/* AJAX requests */
add_action( 'wp_ajax_users_details', 'users_details_callback' );
add_action( 'wp_ajax_nopriv_users_details', 'users_details_callback' );
function users_details_callback() {
  global $wpdb;

  $request = $_POST['request'];
  $response = array();
  // Fetch all records
  if($request == 1){
    $response = $wpdb->get_results("select id,username from users");
  }
  // Fetch record by id
  if($request == 2){
    $userid = $_POST['userid'];
    $response = $wpdb->get_results("select name,username,email from users where id=".$userid);
  }

  echo json_encode($response);
  wp_die();
}

/**
* Custom Image Size.
*/
add_image_size( 'post-thumbnail', 1000, 450, true );



// Excerpt
function wpdocs_custom_excerpt_length( $length ) {
    return 40;
}
add_filter( 'excerpt_length', 'wpdocs_custom_excerpt_length', 999 );

function wpdocs_excerpt_more( $more ) {
    return '...' ;
}
add_filter( 'excerpt_more', 'wpdocs_excerpt_more' );

//remove comments from pages
add_action('init', 'remove_comment_support', 100);

function remove_comment_support() {
remove_post_type_support( 'page', 'comments' );
}
