<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Famous_Insurance
 */
?>

<footer>
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-sm-6">
                <h4>Famous Insurance</h4>
                <ul class="list-inline">
                    <li class="listi-inline-item font-light small"><a href="https://famousinsurance.com.au/wp-content/uploads/2018/07/FAMOUS-Privacy-Policy_PP1119_web.pdf" target="_blank">Privacy Policy</a></li>
                    <li class="listi-inline-item font-light small"><a href="https://famousinsurance.com.au/wp-content/uploads/2018/09/FAMOUS-CollectionStatement_CS1119_web.pdf" target="_blank">Terms &amp; Conditions</a></li>
                    <li class="listi-inline-item font-light small"><a href="https://famousinsurance.com.au/wp-content/uploads/2018/09/FAMOUS-Duty-of-Disclosure.pdf" target="_blank">Duty of Disclosure</a></li>
                    <li class="listi-inline-item font-light small"><a href="https://famousinsurance.com.au/wp-content/uploads/2018/09/FAMOUS-CollectionStatement_CS1119_web.pdf" target="_blank">Collection Statement</a></li>
                    <li class="listi-inline-item font-light small"><a href="https://famousinsurance.com.au/wp-content/uploads/2018/09/FAMOUS-Financial-Services-Guide_After-1Nov2018-1.pdf" target="_blank">Financial Services Guide</a></li>
                </ul>
            </div>
            <div class="col-md-3 col-sm-6">
                <h4>Insurance</h4>
                <ul class="list-inline">
                    <li class="listi-inline-item font-light small"><a href="<?php echo esc_url( home_url( '/type/car-insurance/' ) ); ?>">Car Insurance</a></li>
                    <li class="listi-inline-item font-light small"><a href="<?php echo esc_url( home_url( '/insurance/motorbikes' ) ); ?>">Motorbike Insurance</a></li>
                    <li class="listi-inline-item font-light small"><a href="<?php echo esc_url( home_url( '/insurance/dirtbike' ) ); ?>">Dirt Bike Insurance</a></li>
                    <li class="listi-inline-item font-light small"><a href="<?php echo esc_url( home_url( '/insurance/scooters/' ) ); ?>">Scooter Insurance</a></li>
                </ul>
            </div>
            <div class="col-md-3 col-sm-6">
                <h4>Policies</h4>
                <ul class="list-inline">
                    <li class="listi-inline-item font-light small"><a href="<?php echo esc_url( home_url( '/documents/' ) ); ?>">Policy Documents</a></li>
                    <li class="listi-inline-item font-light small"><a href="<?php echo esc_url( home_url( '/claims/' ) ); ?>">Make A Claim</a></li>
                    <li class="listi-inline-item font-light small"><a href="<?php echo esc_url( home_url( '/payments/' ) ); ?>">Payments</a></li>
                    <!-- <li class="listi-inline-item font-light small"><a href="<?php echo esc_url( home_url( '/get-a-quote/' ) ); ?>">Online Insurance Quote</a></li> -->
                    <li class="listi-inline-item font-light small"><a href="<?php echo esc_url( home_url( '/wp-content/uploads/2018/11/FAMOUS_Complaints_After-1Nov2018indd.pdf' ) ); ?>" target="_blank">Complaints Information Guide</a></li>
                    <li class="listi-inline-item font-light small"><a href="<?php echo esc_url( home_url( '/documents/' ) ); ?>">Family and Domestic Violence Policy</a></li>

                </ul>
            </div>
            <div class="col-md-3 col-sm-6">
                <h4>Follow Us</h4>
                <ul class="list-inline float-left d-flex footer-social">
                    <li class="list-inline-item social align-self-center"><a href="https://www.facebook.com/Famousinsurance" target="_blank">
                            <span class="fa-layers icon-facebook fa-3x">
                                <i class="fas fa-square-full"></i>
                                <i class="fab fa-facebook-f" data-fa-transform="shrink-6"></i>
                            </span>
                        </a>
                    </li>
                    <li class="list-inline-item social align-self-center"><a href="https://www.youtube.com/channel/UCGBTveRFxfAFJK7O9zE4z5Q" target="_blank">
                            <span class="fa-layers icon-youtube fa-3x">
                                <i class="fas fa-square-full"></i>
                                <i class="fab fa-youtube" data-fa-transform="shrink-6"></i>
                            </span>
                        </a>
                    </li>
                    <li class="list-inline-item social align-self-center"><a href="https://twitter.com/famouscarsbikes" target="_blank">
                            <span class="fa-layers icon-twitter fa-3x">
                                <i class="fas fa-square-full"></i>
                                <i class="fab fa-twitter" data-fa-transform="shrink-6"></i>
                            </span>
                        </a>
                    </li>
                    <li class="list-inline-item social align-self-center"><a href="https://www.instagram.com/famouscarsbikes/" target="_blank">
                            <span class="fa-layers icon-instagram fa-3x">
                                <i class="fas fa-square-full"></i>
                                <i class="fab fa-instagram" data-fa-transform="shrink-6"></i>
                            </span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <hr class="hr-white" />
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <p class="font-light xsmall">For Policies incepting on or after 1st October 2017 Insurance is underwritten by RACQ Insurance Limited ABN 50 009 704 152 (‘RACQ’).<br />Conditions, limits and exclusions apply. Insurance underwritten by RACQ Insurance Limited ABN 50 009 704 152 (‘RACQ’). Famous Insurance Agency Pty Ltd ABN 66 168 467 561 is authorised to arrange and issue this insurance as agent of RACQ.<br />Read the <a href="<?php echo get_page_link( get_page_by_title( Documents )->ID ); ?>">PDS</a> before making a purchase decision.</p>
            </div>
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container -->
</footer>



<?php wp_footer(); ?>

<!-- PTEngine Code for Analytics -->
<script type="text/javascript">
    window._pt_sp_2 = [];
    _pt_sp_2.push('setAccount,3cc79e73');
    var _protocol = (("https:" == document.location.protocol) ? " https://" : " http://");
    (function() {
        var atag = document.createElement('script');
        atag.type = 'text/javascript';
        atag.async = true;
        atag.src = _protocol + 'jsv2.ptengine.com/pta.js';
        var stag = document.createElement('script');
        stag.type = 'text/javascript';
        stag.async = true;
        stag.src = _protocol + 'jsv2.ptengine.com/pts.js';
        var s = document.getElementsByTagName('script')[0];
        s.parentNode.insertBefore(atag, s);
        s.parentNode.insertBefore(stag, s);
    })();

</script>
<!-- Google Code for Remarketing Tag -->
<script type="text/javascript">
    /* <![CDATA[ */
    var google_conversion_id = 970200623;
    var google_custom_params = window.google_tag_params;
    var google_remarketing_only = true;
    /* ]]> */

</script>

<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
    <div style="display:inline;">
        <img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/970200623/?value=0&amp;guid=ON&amp;script=0" />
    </div>
</noscript>

<script>
    $(function() {
        $("#viewing-list li").click(function(e) {
            e.preventDefault();
            var elem = $("#inspection-models");

            $(this).toggleClass("active");
            elem.val(
                $("#viewing-list li.active span").map(function() {
                    return $(this).text();
                }).get().join(" ")
            );
        });
    });

</script>

<script type="text/javascript">
    jQuery(document).ready(function() {
        jQuery('#dob').datepicker({
            dateFormat: 'dd/mm/yy',
            changeYear: true,
            yearRange: '-110:-17',
            defaultDate: -18263,
        });
        jQuery('#purchase-date').datepicker({
            dateFormat: 'dd/mm/yy',
            changeYear: true,
            yearRange: '-110:+0',
            defaultDate: +0,
        });
        jQuery('#dd-dob').datepicker({
            dateFormat: 'dd/mm/yy',
            changeYear: true,
            yearRange: '-110:-17',
            defaultDate: -18263,
        });
        jQuery('#dd-purchase-date').datepicker({
            dateFormat: 'dd/mm/yy',
            changeYear: true,
            yearRange: '-110:+0',
            defaultDate: +0,
        });
        jQuery('#pc-dob').datepicker({
            dateFormat: 'dd/mm/yy',
            changeYear: true,
            yearRange: '-110:-17',
            defaultDate: -18263,
        });
        jQuery('#pc-purchase-date').datepicker({
            dateFormat: 'dd/mm/yy',
            changeYear: true,
            yearRange: '-110:+0',
            defaultDate: +0,
        });
        jQuery('#pe-dob').datepicker({
            dateFormat: 'dd/mm/yy',
            changeYear: true,
            yearRange: '-110:-17',
            defaultDate: -18263,
        });
        jQuery('#pe-purchase-date').datepicker({
            dateFormat: 'dd/mm/yy',
            changeYear: true,
            yearRange: '-110:+0',
            defaultDate: +0,
        });
        jQuery('#cc-dob').datepicker({
            dateFormat: 'dd/mm/yy',
            changeYear: true,
            yearRange: '-110:-17',
            defaultDate: -18263,
        });
        jQuery('#cc-purchase-date').datepicker({
            dateFormat: 'dd/mm/yy',
            changeYear: true,
            yearRange: '-110:+0',
            defaultDate: +0,
        });
        jQuery('#tc-dob').datepicker({
            dateFormat: 'dd/mm/yy',
            changeYear: true,
            yearRange: '-110:-17',
            defaultDate: -18263,
        });
        jQuery('#tc-purchase-date').datepicker({
            dateFormat: 'dd/mm/yy',
            changeYear: true,
            yearRange: '-110:+0',
            defaultDate: +0,
        });
        jQuery('#rs-dob').datepicker({
            dateFormat: 'dd/mm/yy',
            changeYear: true,
            yearRange: '-110:-17',
            defaultDate: -18263,
        });
        jQuery('#rs-purchase-date').datepicker({
            dateFormat: 'dd/mm/yy',
            changeYear: true,
            yearRange: '-110:+0',
            defaultDate: +0,
        });
    });

</script>


</body>

</html>
