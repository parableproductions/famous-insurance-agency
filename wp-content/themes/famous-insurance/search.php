<?php
/**
 * The template for displaying search results pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package Famous Insurance
 */

get_header(); ?>

<?php
if(isset($_GET['search-type'])) {
    $type = $_GET['search-type'];
    if($type == 'guidefilter') {
        load_template(TEMPLATEPATH . '/guide-filterresults.php');
    } 
    elseif($type == 'global') {
        load_template(TEMPLATEPATH . '/global-search.php');
    }
}
?>

<?php get_footer(); ?>

<script>
    $(document).ready(function() {
        // Handler for .ready() called.
        $('html, body').animate({
            scrollTop: $('#guidefilter').offset().top
        }, 'slow');
    });

</script>
