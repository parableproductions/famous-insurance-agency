<?php
/**
 * The sidebar containing the main widget area
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Famous_Insurance
 */

if ( ! is_active_sidebar( 'sidebar-1' ) ) {
	return;
}
?>

<div class="card widget-bg sidebar-1">
    <div class="card-body sidebar-1">
        <?php dynamic_sidebar( 'sidebar-1' ); ?>
    </div>
</div>
