<section class="section-red">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h3 class="text-uppercase text-center">
                    Sign up to the Famous Newsletter
                </h3>
                <h3 class="italic text-center">
                    Receive regular updates from Famous Insurance
                </h3>
            </div>
        </div>
        <div class="row">
            <form class="form-inline col-md-12">
                <div class="form-group col-md-5">
                    <label for="inputEmail" class="sr-only">Emaul</label>
                    <input type="email" class="form-control" id="inputEmail" placeholder="Email">
                </div>
                <div class="form-group col-md-5">
                    <label for="inputPassword" class="sr-only">Password</label>
                    <input type="password" class="form-control" id="inputPassword" placeholder="Password">
                </div>
                <div class="col-md-2">
                    <button type="submit" class="btn btn-dark">Sign Up</button>
                </div>
            </form>
        </div>
    </div>
</section>
