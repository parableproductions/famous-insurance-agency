<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Caravan_Camping_Hire
 */

get_header(); ?>
<?php if ( have_rows( 'page_type' ) ): ?>
<?php while ( have_rows( 'page_type' ) ) : the_row(); ?>

<!-- General Layout -->
<?php if ( get_row_layout() == 'general' ) : ?>
<section id="content">
    <div class="container">
        <div class="row">
            <h2 class="page-title screen-reader-text">
                <?php single_post_title(); ?>
            </h2>
            <?php the_sub_field( 'content' ); ?>
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container -->
</section>
<!-- General Layout -->

<!-- About Us -->
<?php elseif ( get_row_layout() == 'about_us' ) : ?>
<?php get_template_part( 'page-templates/about' ); ?>
<!-- About Us -->

<!-- Simple -->
<?php elseif ( get_row_layout() == 'simple' ) : ?>
<?php get_template_part( 'page-templates/simple' ); ?>
<!-- Simple -->

<!-- Quote Form -->
<?php elseif ( get_row_layout() == 'quote_form' ) : ?>
<?php get_template_part( 'page-templates/quote-form' ); ?>
<!-- Quote Form -->

<!-- Contact -->
<?php elseif ( get_row_layout() == 'contact' ) : ?>
<?php get_template_part( 'page-templates/contact' ); ?>
<!-- Contact -->

<!-- Quote -->
<?php elseif ( get_row_layout() == 'quote' ) : ?>
<?php get_template_part( 'page-templates/quote' ); ?>
<!-- Quote -->

<!-- Quote -->
<?php elseif ( get_row_layout() == 'quote_new' ) : ?>
<?php get_template_part( 'page-templates/quote-new' ); ?>
<!-- Quote -->

<!-- Quote Submit -->
<?php elseif ( get_row_layout() == 'quote_submit' ) : ?>
<?php get_template_part( 'page-templates/quote-submit' ); ?>
<!-- Quote Submit -->

<!-- Partnerships -->
<?php elseif ( get_row_layout() == 'partnerships' ) : ?>
<?php get_template_part( 'page-templates/partnerships' ); ?>
<!-- Partnerships -->

<!-- Glasses Guide -->
<?php elseif ( get_row_layout() == 'glasses_guide' ) : ?>
<?php get_template_part( 'page-templates/glasses-guide' ); ?>
<!-- Glasses Guide -->

<!-- Policy Documents -->
<?php elseif ( get_row_layout() == 'policy_documents' ) : ?>
<?php get_template_part( 'page-templates/policy', 'documents' ); ?>
<!-- Policy Documents -->

<!-- Policy Documents -->
<?php elseif ( get_row_layout() == 'nevdis_lookup' ) : ?>
<?php get_template_part( 'page-templates/nevdis', 'lookup' ); ?>
<!-- Policy Documents -->

<!-- Homepage -->
<!-- Header -->
<?php elseif ( get_row_layout() == 'home_page' ) : ?>
<?php get_template_part( 'page-templates/home' ); ?>
<?php endif; ?>

<?php endwhile; ?>
<?php else: ?>
<?php
		while ( have_posts() ) : the_post();

		get_template_part( 'template-parts/content', 'page' );

		// If comments are open or we have at least one comment, load up the comment template.
		if ( comments_open() || get_comments_number() ) :
			comments_template();
		endif;

	endwhile; // End of the loop.
	?>
<?php endif; ?>


<?php get_footer(); ?>
