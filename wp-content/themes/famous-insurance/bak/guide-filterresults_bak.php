<?php
/**
 * The template for displaying search results pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package Famous_Insurance
 */

get_header(); ?>


<?php $top_section_bg = get_sub_field( 'top_section_bg' ); ?>

<?php 
    $image = get_sub_field('top_section_bg' );
    if( !empty($image) ): 
    // vars
    $title = $image['title'];
    $alt = $image['alt'];

	// image
    $size = 'full';
	$thumb = $image['sizes'][ $size ];
	$width = $image['sizes'][ $size . '-width' ];
	$height = $image['sizes'][ $size . '-height' ];
    ?>

<section class="no-padding-top">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <img src="<?php echo $top_section_bg['url']; ?>">
            </div>
        </div>
    </div>
</section>
<?php endif; ?>

<section>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2 class="text-uppercase text-center font-xbold">
                    Filter Results
                </h2>
            </div>
        </div>
    </div>
</section>

<section>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <?php get_template_part( 'guide', 'filter' ); ?>
            </div>
        </div>
    </div>
</section>

<?php if( !empty( $_GET['year'] ) ) { ?>

<section id="content">
    <div class="container">

        <?php                 
			if( !empty( $_GET['make'] ) ) {
				$_make = $_GET['make'];
			}
    if( !empty( $_GET['model'] ) ) {
				$_model = $_GET['model'];
			}
    if( !empty( $_GET['year'] ) ) {
				$_year = $_GET['year'];
			}

			if (isset($_make))
			{
				$meta_query[] =  array(
					'key' => 'gg_make',
					'value' => $_make,
					'type' => 'text',
					'compare' => 'IN'
				);
			}
    
    if (isset($_model))
			{
				$meta_query[] =  array(
					'key' => 'gg_model',
					'value' => $_model,
					'type' => 'text',
					'compare' => 'IN'
				);
			}
    
    if (isset($_year))
			{
				$meta_query[] =  array(
					'key' => 'gg_year',
					'value' => $_year,
					'type' => 'text',
					'compare' => 'IN'
				);
			}

            $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;    

			$args = array(
				's' => get_search_query(),
                'posts_per_page' => 12,
                'post_status' => 'publish',
                'post_type' => 'glasses_guide',
                'order' => 'ASC',
				'meta_query' => $meta_query,
                'paged' => $paged,
			);
                    
					query_posts($args); ?>
        <!-- the loop -->
        <?php if ( have_posts() ) : while (have_posts()) : the_post(); ?>

        <?php $make = get_field('gg_make');
        $make = strtolower($make);
        $make = ucwords($make); ?>

        <?php $model = get_field('gg_model');
        $model = strtolower($model);
        $model = ucwords($model); ?>

        <?php $year = get_field('gg_year');
        $year = strtolower($year);
        $year = ucwords($year); ?>

        <?php $value = get_field('gg_pricehigh');
        $value = strtolower($value);
        $value = ucwords($value); ?>

        <?php $engine = get_field('gg_size');
        $engine = strtolower($engine);
        $engine = ucwords($engine); ?>

        <?php $desc = get_field('gg_index');
        $desc = strtolower($desc);
        $desc = ucwords($desc); ?>

        <?php if( !empty( $_GET['type'] ) ) {
            $type = $_GET['type']; } ?>

        <?php if ($type == 'classic-car') { ?>
        <?php $type = 'classic-car-insurance'; ?>

        <?php } ?>

        <div class="row">
            <div class="col-md-12">
                <a class="text-uppercase" href="<?php echo get_site_url(); ?>/<?php echo $type; ?>/?make=<?php echo $make; ?>&model=<?php echo $model; ?>&make-year=<?php echo $year; ?>&value=<?php echo $value; ?>&engine=<?php echo $engine; ?>&description=<?php echo $desc; ?>" target="_blank">
                    <?php the_title(); ?>
                </a>
            </div>
        </div>
        <?php endwhile; ?>
        <?php endif; ?>
        <?php wp_reset_postdata(); ?>

    </div>
</section>

<?php } ?>

<?php get_footer(); ?>
