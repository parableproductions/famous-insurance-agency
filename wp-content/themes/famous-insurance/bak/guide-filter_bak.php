<?php 
if( !empty( $_GET['make'] ) ) {
$search_make = $_GET['make'];
}
if( !empty( $_GET['model'] ) ) {
$search_model = $_GET['model'];
}
if( !empty( $_GET['year'] ) ) {
$search_year = $_GET['year'];
}
if( !empty( $_GET['type'] ) ) {
$vehicle_type = $_GET['type'];
}

if (isset($search_make))
{
$meta_query[] =  array(
'key' => 'gg_make',
'value' => $search_make,
'type' => 'text',
'compare' => 'IN'
);
}

if (isset($search_model))
{
$meta_query[] =  array(
'key' => 'gg_model',
'value' => $search_model,
'type' => 'text',
'compare' => 'IN'
);
}

if (isset($search_year))
{
$meta_query[] =  array(
'key' => 'gg_year',
'value' => $search_year,
'type' => 'text',
'compare' => 'IN'
);
}
?>

<form role="search" method="get" class="search-form" action="<?php echo home_url( '/' ); ?>">

    <div class="row pt-2">

        <!-- Transient -->
        <?php if ( false === ( $ggfeatured = get_transient( 'gg_vehicle_posts' ) ) ) { ?>

        <?php $ggfeatured = new WP_query (
          array(
        's' => get_search_query(),
        'post_type' => 'glasses_guide',
        'post_status' => 'publish',
        'fields' => 'ids',
        'orderby' => 'name', 
        'order' => 'ASC',
        'no_found_rows' => true,
        'ignore_sticky_posts' => true,
        'posts_per_page' => 1,        
        'meta_query' => $meta_query
    )); ?>

        <?php set_transient( 'gg_vehicle_posts', $ggfeatured, 1 * HOUR_IN_SECONDS ); } ?>

        <?php if ( $ggfeatured->have_posts() ) : ?>

        <?php while ( $ggfeatured->have_posts() ) : $ggfeatured->the_post(); ?>

        <?php $makes[] = get_field('gg_make'); ?>
        <?php $models[] = get_field('gg_model'); ?>
        <?php $variants[] = get_field('gg_variant'); ?>
        <?php $years[] = get_field('gg_year'); ?>

        <?php endwhile; ?>
        <?php else: ?>
        <?php endif; ?>

        <?php wp_reset_postdata(); ?>
        <!-- /Transient -->

        <?php
        $makes = array_unique($makes);
        $makes = array_filter($makes);
        $makes = array_values($makes);
        ?>
        <?php
        $models = array_unique($models);
        $models = array_filter($models);
        $models = array_values($models);
        ?>
        <?php
        $years = array_unique($years);
        $years = array_filter($years);
        $years = array_values($years);
        ?>

        <div class="col-md-4">

            <select name="make" class="form-control" id="make" onchange="this.form.submit()">
                <option value="">
                    All Makes
                </option>
                <?php foreach ( $makes as $make ) { ?>
                <?php $make = strtolower($make); ?>
                <?php $make = ucwords($make); ?>
                <option value="<?php echo $make; ?>" <?php if ($make == $search_make) { ?>selected="selected" <?php } ?>><?php echo $make; ?></option>
                <?php } ?>
            </select>
        </div>
        <div class="col-md-4">
            <select name="model" class="form-control" id="model" <?php if( empty( $_GET['make'] ) ) { ?>disabled="disabled" <?php } ?> onchange="this.form.submit()">
                <option value="">
                    All Models
                </option>
                <?php foreach ( $models as $model ) { ?>
                <?php $model = strtolower($model); ?>
                <?php $model = ucwords($model); ?>
                <option value="<?php echo $model; ?>" <?php if ($model == $search_model) { ?>selected="selected" <?php } ?>><?php echo $model; ?></option>
                <?php } ?>
            </select>
        </div>
        <div class="col-md-4">
            <select name="year" class="form-control" id="year" <?php if( empty( $_GET['model'] ) ) { ?>disabled="disabled" <?php } ?> onchange="this.form.submit()">
                <option value="">
                    All Years
                </option>
                <?php foreach ( $years as $year ) { ?>
                <option value="<?php echo $year; ?>" <?php if ($year == $search_year) { ?>selected="selected" <?php } ?>><?php echo $year; ?></option>
                <?php } ?>
            </select>
        </div>
    </div>

    <input type="hidden" class="form-control keywords" name="s" id="search" placeholder="Enter Keywords..." value="<?php get_search_query(); ?>">

    <input type="hidden" name="type" value="<?php echo $vehicle_type; ?>" />

    <input type="hidden" name="search-type" value="guidefilter" />

</form>

<?php
global $wpdb;
$customers = $wpdb->get_results("SELECT * FROM wp_glasses_guide");
?>

<?php foreach($customers as $customer){ ?>

<?php $makes[] = $customer->gg_make; ?>
<?php $models[] = $customer->gg_model; ?>
<?php $years[] = $customer->gg_year; ?>

<?php
        $makes = array_unique($makes);
        $makes = array_filter($makes);
        $makes = array_values($makes);
                                       
        $models = array_unique($models);
        $models = array_filter($models);
        $models = array_values($models);
                                       
        $years = array_unique($years);
        $years = array_filter($years);
        $years = array_values($years);
        ?>

<?php } ?>

<?php foreach ( $makes as $make ) { ?>
<?php $make = strtolower($make); ?>
<?php $make = ucwords($make); ?>
<option value="<?php echo $make; ?>" <?php if ($make == $search_make) { ?>selected="selected" <?php } ?>><?php echo $make; ?></option>
<?php } ?>
