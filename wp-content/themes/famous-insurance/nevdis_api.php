<?php
class nevdis_api {
	private $apikey = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJhZDg4MWRiOS1hNjMzLTRhYTYtOTlmOC1lM2RlOWVmM2Y3OTgiLCJuYW1lIjoiRmFtb3VzIEluc3VyYW5jZSIsImlhdCI6MTUxNjIzOTAyMn0.OBsAG9nranheG8utJCt0ap7IaK0qsZKSKtkftuhV7J0';
	private $apiurl = 'https://ubuxgyols2.execute-api.ap-southeast-2.amazonaws.com/prod/';
	
	/*
	* Initiate API Request
	* @param string $data = String of API request data
	* 
	* @return array 
	*
	*
	*/
	protected function init_request($data){
		$ch = curl_init($this->apiurl);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
			'Authorization: JWT '. $this->apikey,
		));
		$result = curl_exec($ch);
		
		return $result;
	}

	protected function clean_api_result($data, $type = NULL){
		$data = json_decode($data, true);
		if($type == 'vin'){
			$result = $data['data']['nevdisVINSearch_v2'][0];
		}
		else{
			$result = $data['data']['nevdisPlateSearch_v2'][0];
		}
		return $result;
	}
	
	public function request_data($query,$type = NULL){
		if($type == 'vin'){
			$vin = $query['vin'];
			$data = '{ "query": "{ nevdisVINSearch_v2(vin: \"'.$vin.'\") { vin plate { number state } make model engine_number vehicle_type body_type colour }}"}';
			$result = $this->init_request($data);
			return $this->clean_api_result($result, 'vin');
		}
		else{
			$state = $query['state'];
			$plate = $query['plate'];
			$data = '{ "query": "{ nevdisPlateSearch_v2(plate: \"'.$plate.'\", state:'.$state.') { vin plate { number state } make model engine_number vehicle_type body_type colour }}"}';
			$result = $this->init_request($data,'rego');
			return $this->clean_api_result($result);
		}
		return NULL;
	}
}
?>
