<?php
define('WP_CACHE', true); // Added by WP Rocket
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */
// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'famous');
/** MySQL database username */
define('DB_USER', 'root');
/** MySQL database password */
define('DB_PASSWORD', 'root');
/** MySQL hostname */
define('DB_HOST', 'localhost');
/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');
/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');
/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         ']:I~73X6_d@;x3S *9PLK9$P*<(YF)=mt_D ,XCo_=RdR@W$U)[8bLWC7/mtpw1g');
define('SECURE_AUTH_KEY',  'r;UJ%n32>Y!bt1w,iLxoHc}MuD-SJ~}>S+NYa%f?b?BQ,SRo&Q(9`Kl 04J!su)j');
define('LOGGED_IN_KEY',    'eC8apq duevhL1l9iR5SNk%|rkCt|:3=cOLx^2;u,a^r9n6AGK1L#*Naf.jlq2lV');
define('NONCE_KEY',        'HF+m4~ ynHs[+t+%y~&(SZ(X>=L|bBfGbd2R39VmOKVeg5aCdTHNB|^<N3{`VeA8');
define('AUTH_SALT',        'c@o:lIE-9bOsu-h+kYy@a)#&KXH7:6!I/&B:A.Hx@NV+jH0I#@Q{-mt|JT$[yEP|');
define('SECURE_AUTH_SALT', ')Db=Z# XSxVI{@>y SA(2L-QYI&;<ar4z0(LSh,LuDIbwgI|8w`M]Ti;G@N[#f37');
define('LOGGED_IN_SALT',   '}D-L%{i %W[2gX4A:{R[u+C*laB,rWD$Yly9e@S,)o1ksxPQ8+iN%qe+?h|2w?ko');
define('NONCE_SALT',       'IvVs;h{dShHKRfYm h|7-I+5/|g_CKq02#w?GVh[LrNE;|0h|5nW8U*NNq[%,6^+');
/**#@-*/
/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';
/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);
/* That's all, stop editing! Happy blogging. */
/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');
/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
